# dv-runtime

C++ event-based processing framework for neuromorphic cameras, targeting embedded and desktop systems. <br />

Get started documentation: https://inivation.gitlab.io/dv-docs/

# History

The Dynamic Vision (DV) toolkit is the successor to the cAER software, providing a better platform
for development of applications and interaction with event-based sensors, thanks to a better,
simpler API and a first-class graphical user interface (GUI).
The last release of cAER can be retrieved from our version control here:

https://gitlab.com/inivation/dv-runtime/tags/caer-1.1.2

# Dependencies:

NOTE: if you intend to install the latest git master checkout, please also make sure to use the latest
libcaer git master checkout, as we often depend on new features of the libcaer development version.

Linux, MacOS X or Windows (for Windows build instructions see README.Windows) <br />
gcc >= 7.0 or clang >= 7.0 <br />
cmake >= 3.10.0 <br />
Boost >= 1.50 (with system, filesystem, iostreams, program_options) <br />
OpenSSL (for Boost.ASIO SSL) <br />
OpenCV >= 3.1.0 <br />
libcaer >= 3.2.2 <br />
liblz4 <br />
libzstd <br />
Optional: tcmalloc >= 2.2 (faster memory allocation) <br />
Optional: SFML >= 2.3.0 (deprecated visualizer module, use dv-gui instead) <br />

On Fedora Linux: $ sudo dnf install @c-development cmake pkgconfig boost-devel openssl-devel opencv-devel lz4-devel libzstd-devel libcaer-devel <br />
On Ubuntu Linux: $ sudo apt-get install build-essential cmake pkg-config libboost-all-dev libssl-dev libopencv-dev libopencv-contrib-dev liblz4-dev libzstd-dev libcaer-dev <br />
On MacOS X (using Homebrew): $ brew install cmake pkg-config boost openssl opencv lz4 zstd libcaer <br />

# Installation

1) configure:
<br />
$ cmake -DCMAKE_INSTALL_PREFIX=/usr <OPTIONS> .
<br />
<br />
The following options are currently supported: <br />
-DENABLE_TCMALLOC=1 -- Enables usage of TCMalloc from Google to allocate memory faster. <br />
-DENABLE_VISUALIZER=1 -- Open separate windows in which to visualize data (deprecated, use dv-gui instead). <br />
<br />
2) build:
<br />
$ make
<br />
<br />
3) install:
<br />
$ sudo make install
<br />

# Usage

You will need an XML file that specifies which and how modules
should be interconnected. We recommend using dv-gui to create
the first configuration and interact with it. <br />
Please follow our online documentation and tutorials to get started:
<br />
https://inivation.gitlab.io/dv-docs/
<br />

$ dv-runtime (see online docs for more information) <br />
$ dv-control (command-line run-time control program) <br />

# Help

Please use our GitLab bug tracker to report issues and bugs, or
our Google Groups mailing list for discussions and announcements.

BUG TRACKER: https://gitlab.com/inivation/dv-runtime/issues/

MAILING LIST: https://groups.google.com/d/forum/dv-users/

BUILD STATUS: https://gitlab.com/inivation/dv-runtime/pipelines/

CODE ANALYSIS: https://sonarcloud.io/dashboard?id=com.inivation.dv-runtime
