#ifndef DVCONFIG_H_
#define DVCONFIG_H_

#ifdef __cplusplus

#	include <cerrno>
#	include <cinttypes>
#	include <cstdint>
#	include <cstdio>
#	include <cstdlib>

#else

#	include <errno.h>
#	include <inttypes.h>
#	include <stdbool.h>
#	include <stdint.h>
#	include <stdio.h>
#	include <stdlib.h>

#endif

#if defined(_WIN32) || defined(__CYGWIN__)
#	ifdef __GNUC__
#		define LIB_PUBLIC_VISIBILITY __attribute__((dllexport))
#	else
#		define LIB_PUBLIC_VISIBILITY __declspec(dllexport)
#	endif
#else
#	define LIB_PUBLIC_VISIBILITY __attribute__((visibility("default")))
#endif

#ifdef __cplusplus
extern "C" {
#endif

// dv::Config Node
typedef struct dv_config_node *dvConfigNode;
typedef const struct dv_config_node *dvConfigNodeConst;

enum dvConfigAttributeType {
	DVCFG_TYPE_UNKNOWN = -1,
	DVCFG_TYPE_BOOL    = 0,
	DVCFG_TYPE_INT     = 1,
	DVCFG_TYPE_LONG    = 2,
	DVCFG_TYPE_FLOAT   = 3,
	DVCFG_TYPE_DOUBLE  = 4,
	DVCFG_TYPE_STRING  = 5,
};

union dvConfigAttributeValue {
	bool boolean;
	int32_t iint;
	int64_t ilong;
	float ffloat;
	double ddouble;
	char *string;
};

union dvConfigAttributeRange {
	int32_t intRange;
	int64_t longRange;
	float floatRange;
	double doubleRange;
	int32_t stringRange;
};

struct dvConfigAttributeRanges {
	union dvConfigAttributeRange min;
	union dvConfigAttributeRange max;
};

enum dvConfigAttributeFlags {
	DVCFG_FLAGS_NORMAL    = 0,
	DVCFG_FLAGS_READ_ONLY = 1,
	DVCFG_FLAGS_NO_EXPORT = 2,
	DVCFG_FLAGS_IMPORTED  = 4,
};

enum dvConfigNodeEvents {
	DVCFG_NODE_CHILD_ADDED   = 0,
	DVCFG_NODE_CHILD_REMOVED = 1,
};

enum dvConfigAttributeEvents {
	DVCFG_ATTRIBUTE_ADDED           = 0,
	DVCFG_ATTRIBUTE_MODIFIED        = 1,
	DVCFG_ATTRIBUTE_REMOVED         = 2,
	DVCFG_ATTRIBUTE_MODIFIED_CREATE = 3,
};

typedef void (*dvConfigNodeChangeListener)(
	dvConfigNode node, void *userData, enum dvConfigNodeEvents event, const char *changeNode);

typedef void (*dvConfigAttributeChangeListener)(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
	const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue);

LIB_PUBLIC_VISIBILITY const char *dvConfigNodeGetName(dvConfigNode node);
LIB_PUBLIC_VISIBILITY const char *dvConfigNodeGetPath(dvConfigNode node);

/**
 * This returns a reference to a node, and as such must be carefully mediated with
 * any dvConfigNodeRemoveNode() calls.
 */
LIB_PUBLIC_VISIBILITY dvConfigNode dvConfigNodeGetParent(dvConfigNode node);
/**
 * Remember to free the resulting array. This returns references to nodes,
 * and as such must be carefully mediated with any dvConfigNodeRemoveNode() calls.
 */
LIB_PUBLIC_VISIBILITY dvConfigNode *dvConfigNodeGetChildren(
	dvConfigNode node, size_t *numChildren); // Walk all children.

LIB_PUBLIC_VISIBILITY void dvConfigNodeAddNodeListener(
	dvConfigNode node, void *userData, dvConfigNodeChangeListener node_changed);
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveNodeListener(
	dvConfigNode node, void *userData, dvConfigNodeChangeListener node_changed);
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveAllNodeListeners(dvConfigNode node);

LIB_PUBLIC_VISIBILITY void dvConfigNodeAddAttributeListener(
	dvConfigNode node, void *userData, dvConfigAttributeChangeListener attribute_changed);
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveAttributeListener(
	dvConfigNode node, void *userData, dvConfigAttributeChangeListener attribute_changed);
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveAllAttributeListeners(dvConfigNode node);

/**
 * Careful, only use if no references exist to this node and all its children.
 * References are created by dvConfigTreeGetNode(), dvConfigNodeGetRelativeNode(),
 * dvConfigNodeGetParent() and dvConfigNodeGetChildren().
 */
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveNode(dvConfigNode node);
/**
 * Careful, only use if no references exist to this node's children.
 * References are created by dvConfigTreeGetNode(), dvConfigNodeGetRelativeNode(),
 * dvConfigNodeGetParent() and dvConfigNodeGetChildren().
 */
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveSubTree(dvConfigNode node);
LIB_PUBLIC_VISIBILITY void dvConfigNodeClearSubTree(dvConfigNode startNode, bool clearStartNode);

LIB_PUBLIC_VISIBILITY void dvConfigNodeCopy(dvConfigNode source, dvConfigNode destination);

LIB_PUBLIC_VISIBILITY void dvConfigNodeCreateAttribute(dvConfigNode node, const char *key,
	enum dvConfigAttributeType type, union dvConfigAttributeValue defaultValue,
	const struct dvConfigAttributeRanges ranges, int flags, const char *description);
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveAttribute(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type);
LIB_PUBLIC_VISIBILITY void dvConfigNodeRemoveAllAttributes(dvConfigNode node);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeExistsAttribute(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type);
LIB_PUBLIC_VISIBILITY bool dvConfigNodePutAttribute(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type, union dvConfigAttributeValue value);
LIB_PUBLIC_VISIBILITY union dvConfigAttributeValue dvConfigNodeGetAttribute(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeUpdateReadOnlyAttribute(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type, union dvConfigAttributeValue value);

LIB_PUBLIC_VISIBILITY void dvConfigNodeCreateBool(
	dvConfigNode node, const char *key, bool defaultValue, int flags, const char *description);
LIB_PUBLIC_VISIBILITY bool dvConfigNodePutBool(dvConfigNode node, const char *key, bool value);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeGetBool(dvConfigNode node, const char *key);
LIB_PUBLIC_VISIBILITY void dvConfigNodeCreateInt(dvConfigNode node, const char *key, int32_t defaultValue,
	int32_t minValue, int32_t maxValue, int flags, const char *description);
LIB_PUBLIC_VISIBILITY bool dvConfigNodePutInt(dvConfigNode node, const char *key, int32_t value);
LIB_PUBLIC_VISIBILITY int32_t dvConfigNodeGetInt(dvConfigNode node, const char *key);
LIB_PUBLIC_VISIBILITY void dvConfigNodeCreateLong(dvConfigNode node, const char *key, int64_t defaultValue,
	int64_t minValue, int64_t maxValue, int flags, const char *description);
LIB_PUBLIC_VISIBILITY bool dvConfigNodePutLong(dvConfigNode node, const char *key, int64_t value);
LIB_PUBLIC_VISIBILITY int64_t dvConfigNodeGetLong(dvConfigNode node, const char *key);
LIB_PUBLIC_VISIBILITY void dvConfigNodeCreateFloat(dvConfigNode node, const char *key, float defaultValue,
	float minValue, float maxValue, int flags, const char *description);
LIB_PUBLIC_VISIBILITY bool dvConfigNodePutFloat(dvConfigNode node, const char *key, float value);
LIB_PUBLIC_VISIBILITY float dvConfigNodeGetFloat(dvConfigNode node, const char *key);
LIB_PUBLIC_VISIBILITY void dvConfigNodeCreateDouble(dvConfigNode node, const char *key, double defaultValue,
	double minValue, double maxValue, int flags, const char *description);
LIB_PUBLIC_VISIBILITY bool dvConfigNodePutDouble(dvConfigNode node, const char *key, double value);
LIB_PUBLIC_VISIBILITY double dvConfigNodeGetDouble(dvConfigNode node, const char *key);
LIB_PUBLIC_VISIBILITY void dvConfigNodeCreateString(dvConfigNode node, const char *key, const char *defaultValue,
	int32_t minLength, int32_t maxLength, int flags, const char *description);
LIB_PUBLIC_VISIBILITY bool dvConfigNodePutString(dvConfigNode node, const char *key, const char *value);
LIB_PUBLIC_VISIBILITY char *dvConfigNodeGetString(dvConfigNode node, const char *key);

LIB_PUBLIC_VISIBILITY bool dvConfigNodeExportNodeToXML(dvConfigNode node, int fd, bool exportAll);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeExportSubTreeToXML(dvConfigNode node, int fd, bool exportAll);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeImportNodeFromXML(dvConfigNode node, int fd, bool strict);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeImportSubTreeFromXML(dvConfigNode node, int fd, bool strict);

LIB_PUBLIC_VISIBILITY char *dvConfigNodeExportNodeToXMLString(dvConfigNode node, bool exportAll);
LIB_PUBLIC_VISIBILITY char *dvConfigNodeExportSubTreeToXMLString(dvConfigNode node, bool exportAll);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeImportNodeFromXMLString(dvConfigNode node, const char *xmlStr, bool strict);
LIB_PUBLIC_VISIBILITY bool dvConfigNodeImportSubTreeFromXMLString(dvConfigNode node, const char *xmlStr, bool strict);

LIB_PUBLIC_VISIBILITY bool dvConfigNodeStringToAttributeConverter(
	dvConfigNode node, const char *key, const char *type, const char *value, bool overrideReadOnly);
LIB_PUBLIC_VISIBILITY const char **dvConfigNodeGetChildNames(dvConfigNode node, size_t *numNames);
LIB_PUBLIC_VISIBILITY const char **dvConfigNodeGetAttributeKeys(dvConfigNode node, size_t *numKeys);
LIB_PUBLIC_VISIBILITY enum dvConfigAttributeType dvConfigNodeGetAttributeType(dvConfigNode node, const char *key);
LIB_PUBLIC_VISIBILITY struct dvConfigAttributeRanges dvConfigNodeGetAttributeRanges(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type);
LIB_PUBLIC_VISIBILITY int dvConfigNodeGetAttributeFlags(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type);
LIB_PUBLIC_VISIBILITY char *dvConfigNodeGetAttributeDescription(
	dvConfigNode node, const char *key, enum dvConfigAttributeType type);

LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeModifierButton(
	dvConfigNode node, const char *key, const char *buttonLabel);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeModifierListOptions(
	dvConfigNode node, const char *key, const char *listOptions, bool allowMultipleSelections);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeModifierFileChooser(
	dvConfigNode node, const char *key, const char *typeAndExtensions);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeModifierUnit(
	dvConfigNode node, const char *key, const char *unitInformation);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeModifierPriorityAttributes(
	dvConfigNode node, const char *priorityAttributes);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeModifierGUISupport(dvConfigNode node);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeBooleanReset(dvConfigNode node, const char *key);

LIB_PUBLIC_VISIBILITY bool dvConfigNodeExistsRelativeNode(dvConfigNode node, const char *nodePath);
/**
 * This returns a reference to a node, and as such must be carefully mediated with
 * any dvConfigNodeRemoveNode() calls.
 */
LIB_PUBLIC_VISIBILITY dvConfigNode dvConfigNodeGetRelativeNode(dvConfigNode node, const char *nodePath);

// dv::Config Helper functions
LIB_PUBLIC_VISIBILITY const char *dvConfigHelperTypeToStringConverter(enum dvConfigAttributeType type);
LIB_PUBLIC_VISIBILITY enum dvConfigAttributeType dvConfigHelperStringToTypeConverter(const char *typeString);
LIB_PUBLIC_VISIBILITY char *dvConfigHelperValueToStringConverter(
	enum dvConfigAttributeType type, union dvConfigAttributeValue value);
LIB_PUBLIC_VISIBILITY union dvConfigAttributeValue dvConfigHelperStringToValueConverter(
	enum dvConfigAttributeType type, const char *valueString);
LIB_PUBLIC_VISIBILITY char *dvConfigHelperFlagsToStringConverter(int flags);
LIB_PUBLIC_VISIBILITY int dvConfigHelperStringToFlagsConverter(const char *flagsString);
LIB_PUBLIC_VISIBILITY char *dvConfigHelperRangesToStringConverter(
	enum dvConfigAttributeType type, struct dvConfigAttributeRanges ranges);
LIB_PUBLIC_VISIBILITY struct dvConfigAttributeRanges dvConfigHelperStringToRangesConverter(
	enum dvConfigAttributeType type, const char *rangesString);

// dv::Config Tree
typedef struct dv_config_tree *dvConfigTree;
typedef void (*dvConfigTreeErrorLogCallback)(const char *msg, bool fatal);

LIB_PUBLIC_VISIBILITY dvConfigTree dvConfigTreeGlobal(void);
LIB_PUBLIC_VISIBILITY dvConfigTree dvConfigTreeNew(void);
LIB_PUBLIC_VISIBILITY void dvConfigTreeDelete(dvConfigTree tree);
LIB_PUBLIC_VISIBILITY void dvConfigTreeErrorLogCallbackSet(dvConfigTreeErrorLogCallback error_log_cb);
LIB_PUBLIC_VISIBILITY dvConfigTreeErrorLogCallback dvConfigTreeErrorLogCallbackGet(void);

LIB_PUBLIC_VISIBILITY bool dvConfigTreeExistsNode(dvConfigTree st, const char *nodePath);
/**
 * This returns a reference to a node, and as such must be carefully mediated with
 * any dvConfigNodeRemoveNode() calls.
 */
LIB_PUBLIC_VISIBILITY dvConfigNode dvConfigTreeGetNode(dvConfigTree st, const char *nodePath);

typedef union dvConfigAttributeValue (*dvConfigAttributeUpdater)(
	void *userData, const char *key, enum dvConfigAttributeType type);

LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeUpdaterAdd(dvConfigNode node, const char *key,
	enum dvConfigAttributeType type, dvConfigAttributeUpdater updater, void *updaterUserData, bool runOnce);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeUpdaterRemove(dvConfigNode node, const char *key,
	enum dvConfigAttributeType type, dvConfigAttributeUpdater updater, void *updaterUserData);
LIB_PUBLIC_VISIBILITY void dvConfigNodeAttributeUpdaterRemoveAll(dvConfigNode node);
LIB_PUBLIC_VISIBILITY void dvConfigTreeAttributeUpdaterRemoveAll(dvConfigTree tree);
LIB_PUBLIC_VISIBILITY bool dvConfigTreeAttributeUpdaterRun(dvConfigTree tree);

/**
 * Listener must be able to deal with userData being NULL at any moment.
 * This can happen due to concurrent changes from this setter.
 */
LIB_PUBLIC_VISIBILITY void dvConfigTreeGlobalNodeListenerSet(
	dvConfigTree tree, dvConfigNodeChangeListener node_changed, void *userData);
/**
 * Listener must be able to deal with userData being NULL at any moment.
 * This can happen due to concurrent changes from this setter.
 */
LIB_PUBLIC_VISIBILITY void dvConfigTreeGlobalAttributeListenerSet(
	dvConfigTree tree, dvConfigAttributeChangeListener attribute_changed, void *userData);

#ifdef __cplusplus
}
#endif

#endif /* DVCONFIG_H_ */
