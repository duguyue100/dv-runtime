#ifndef DV_SDK_TRIGGER_HPP
#define DV_SDK_TRIGGER_HPP

#include "trigger_base.hpp"
#include "wrappers.hpp"

namespace dv {

template<>
class InputVectorDataWrapper<dv::TriggerPacket, dv::TriggerT>
	: public _InputVectorDataWrapperCommon<dv::TriggerPacket, dv::TriggerT> {
public:
	InputVectorDataWrapper(std::shared_ptr<const NativeType> p) :
		_InputVectorDataWrapperCommon<dv::TriggerPacket, dv::TriggerT>(std::move(p)) {
	}
};

template<>
class OutputVectorDataWrapper<dv::TriggerPacket, dv::TriggerT>
	: public _OutputVectorDataWrapperCommon<dv::TriggerPacket, dv::TriggerT> {
public:
	OutputVectorDataWrapper(NativeType *p, dvModuleData m, const std::string &n) :
		_OutputVectorDataWrapperCommon<dv::TriggerPacket, dv::TriggerT>(p, m, n) {
	}

	// Un-hide copy assignment.
	using _OutputDataWrapperCommon<dv::TriggerPacket>::operator=;
};

} // namespace dv

#endif // DV_SDK_TRIGGER_HPP
