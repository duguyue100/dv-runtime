#ifndef DV_SDK_EVENT_HPP
#define DV_SDK_EVENT_HPP

#include "event_base.hpp"
#include "wrappers.hpp"

// Allow disabling of OpenCV requirement.
#ifndef DV_API_OPENCV_SUPPORT
#	define DV_API_OPENCV_SUPPORT 1
#endif

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
#	include <opencv2/core.hpp>
#	include <opencv2/core/utility.hpp>
#endif

namespace dv {

template<>
class InputVectorDataWrapper<dv::EventPacket, dv::Event>
	: public _InputVectorDataWrapperCommon<dv::EventPacket, dv::Event> {
public:
	InputVectorDataWrapper(std::shared_ptr<const NativeType> p) :
		_InputVectorDataWrapperCommon<dv::EventPacket, dv::Event>(std::move(p)) {
	}
};

template<>
class OutputVectorDataWrapper<dv::EventPacket, dv::Event>
	: public _OutputVectorDataWrapperCommon<dv::EventPacket, dv::Event> {
public:
	OutputVectorDataWrapper(NativeType *p, dvModuleData m, const std::string &n) :
		_OutputVectorDataWrapperCommon<dv::EventPacket, dv::Event>(p, m, n) {
	}

	// Un-hide copy assignment.
	using _OutputDataWrapperCommon<dv::EventPacket>::operator=;
};

/**
 * Describes an input for event packets. Offers convenience functions to obtain informations
 * about the event input as well as to get the event data.
 */
template<>
class RuntimeVectorInput<dv::EventPacket, dv::Event> : public _RuntimeVectorInputCommon<dv::EventPacket, dv::Event> {
public:
	RuntimeVectorInput(const std::string &name, dvModuleData moduleData) :
		_RuntimeVectorInputCommon<dv::EventPacket, dv::Event>(name, moduleData) {
	}

	/**
	 * Returns an iterable container of the latest events that arrived at this input.
	 * @return An iterable container of the newest events.
	 */
	const InputVectorDataWrapper<dv::EventPacket, dv::Event> events() const {
		return (data());
	}

	/**
	 * @return The width of the input region in pixels. Any event on this input will have a x-coordinate
	 * smaller than the return value of this function.
	 */
	int sizeX() const {
		return (infoNode().getInt("sizeX"));
	}

	/**
	 * @return The height of the input region in pixels. Any event on this input will have a y-coordinate
	 * smaller than the return value of this function
	 */
	int sizeY() const {
		return (infoNode().getInt("sizeY"));
	}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	/**
	 * @return the input region size in pixels as an OpenCV size object
	 */
	const cv::Size size() const {
		return (cv::Size(sizeX(), sizeY()));
	}
#endif
};

/**
 * Specialization of the runtime output for event outputs.
 * Provides convenience setup functions for setting up the event output
 */
template<>
class RuntimeVectorOutput<dv::EventPacket, dv::Event> : public _RuntimeVectorOutputCommon<dv::EventPacket, dv::Event> {
public:
	RuntimeVectorOutput(const std::string &name, dvModuleData moduleData) :
		_RuntimeVectorOutputCommon<dv::EventPacket, dv::Event>(name, moduleData) {
	}

	OutputVectorDataWrapper<dv::EventPacket, dv::Event> events() {
		return (data());
	}

	/**
	 * Sets up this event output by setting the provided arguments to the output info node
	 * @param sizeX The width of this event output
	 * @param sizeY The height of this event output
	 * @param originDescription A description that describes the original generator of the data
	 */
	void setup(int sizeX, int sizeY, const std::string &originDescription) {
		createSourceAttribute(originDescription);
		createSizeAttributes(sizeX, sizeY);
	}

	/**
	 * Sets this event output up with the same parameters as the supplied input.
	 * @param input An input to copy the information from
	 */
	template<typename U> void setup(const RuntimeInput<U> &input) {
		input.infoNode().copyTo(infoNode());

		// Check that required attributes exist at least.
		getOriginDescription();
		sizeX();
		sizeY();
	}

	/**
	 * Sets this event output up with the same parameters as the supplied vector input.
	 * @param input A vector input to copy the information from
	 */
	template<typename U, typename TT> void setup(const RuntimeVectorInput<U, TT> &input) {
		input.infoNode().copyTo(infoNode());

		// Check that required attributes exist at least.
		getOriginDescription();
		sizeX();
		sizeY();
	}

	/**
	 * @return The width of the input region in pixels. Any event on this input will have a x-coordinate
	 * smaller than the return value of this function.
	 */
	int sizeX() const {
		return (infoNode().getInt("sizeX"));
	}

	/**
	 * @return The height of the input region in pixels. Any event on this input will have a y-coordinate
	 * smaller than the return value of this function
	 */
	int sizeY() const {
		return (infoNode().getInt("sizeY"));
	}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	/**
	 * @return the input region size in pixels as an OpenCV size object
	 */
	const cv::Size size() const {
		return (cv::Size(sizeX(), sizeY()));
	}
#endif
};

} // namespace dv

#endif // DV_SDK_EVENT_HPP
