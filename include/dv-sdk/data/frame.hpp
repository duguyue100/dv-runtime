#ifndef DV_SDK_FRAME_HPP
#define DV_SDK_FRAME_HPP

#include "frame_base.hpp"
#include "wrappers.hpp"

// Allow disabling of OpenCV requirement.
#ifndef DV_API_OPENCV_SUPPORT
#	define DV_API_OPENCV_SUPPORT 1
#endif

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
#	include <opencv2/core.hpp>
#	include <opencv2/core/utility.hpp>
#endif

namespace dv {

template<> class InputDataWrapper<dv::Frame> : public _InputDataWrapperCommon<dv::Frame> {
private:
#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	std::shared_ptr<const cv::Mat> matPtr;
#endif

public:
	InputDataWrapper(std::shared_ptr<const NativeType> p) : _InputDataWrapperCommon<dv::Frame>(std::move(p)) {
#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
		// Use custom deleter to bind life-time of main data 'ptr' to OpenCV 'matPtr'.
		if (ptr) {
			matPtr = std::shared_ptr<const cv::Mat>{new cv::Mat(ptr->sizeY, ptr->sizeX, static_cast<int>(ptr->format),
														const_cast<uint8_t *>(ptr->pixels.data())),
				[ptr = ptr](const cv::Mat *mp) { delete mp; }};
		}
#endif
	}

	/**
	 * Returns the dv frame format of the current frame
	 * @return the dv frame format of the current frame
	 */
	dv::FrameFormat format() const noexcept {
		return ptr->format;
	}

	/**
	 * The width of the current frame
	 * @return the width of the current frame
	 */
	int16_t sizeX() const noexcept {
		return ptr->sizeX;
	}

	/**
	 * The height of the current frame
	 * @return the height of the current frame
	 */
	int16_t sizeY() const noexcept {
		return ptr->sizeY;
	}

	/**
	 * Returns the position in x of the region of interest (ROI) of the current frame
	 * @return the x coordinate of the start of the region of interest
	 */
	int16_t positionX() const noexcept {
		return ptr->positionX;
	}

	/**
	 * Returns the position in y of the region of interest (ROI) of the current frame
	 * @return the y coordinate of the start of the region of interest
	 */
	int16_t positionY() const noexcept {
		return ptr->positionY;
	}

	/**
	 * Access a vector of pixels in this frame
	 * @return A const reference to the vector of pixels in this frame
	 */
	const dv::cvector<uint8_t> &pixels() const noexcept {
		return ptr->pixels;
	}

	/**
	 * Returns the timestamp of this frame
	 * @return The timestamp of this frame
	 */
	int64_t timestamp() const noexcept {
		return ptr->timestamp;
	}

	/**
	 * Returns the timestamp of the start of this frame
	 */
	int64_t timestampStartOfFrame() const noexcept {
		return ptr->timestampStartOfFrame;
	}

	/**
	 * Returns the timestamp of the end of this frame
	 * @return timestamp of the end of this frame
	 */
	int64_t timestampEndOfFrame() const noexcept {
		return ptr->timestampEndOfFrame;
	}

	/**
	 * Returns the timestamp of the start of exposure for this frame
	 * @return the timestamp of the end of exposure for this frame
	 */
	int64_t timestampStartOfExposure() const noexcept {
		return ptr->timestampStartOfExposure;
	}

	/**
	 * Returns the timestamp of the end of exposure for this frame
	 * @return the timestamp of the end of exposure for this frame
	 */
	int64_t timestampEndOfExposure() const noexcept {
		return ptr->timestampEndOfExposure;
	}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	/**
	 * Return a read-only OpenCV Mat representing this frame.
	 *
	 * @return a read-only OpenCV Mat pointer
	 */
	std::shared_ptr<const cv::Mat> getMatPointer() const noexcept {
		return (matPtr);
	}

	/**
	 * Returns the size of the current frame. This is generally the same as the input
	 * dimension, but depending on the ROI setting, can be less
	 * @return The size of the current frame
	 */
	cv::Size size() const {
		return cv::Size(sizeX(), sizeY());
	}

	/**
	 * Returns the position of the ROI of the current frame.
	 * @return The position of the start of the ROI of the current frame
	 */
	cv::Point position() const {
		return cv::Point(positionX(), positionY());
	}

	/**
	 * Returns the rectangle defining the region of interest of the current frame
	 * @return The rectangle describing the region of interest of the current frame
	 */
	cv::Rect roi() const {
		return cv::Rect(position(), size());
	}
#endif
};

template<> class OutputDataWrapper<dv::Frame> : public _OutputDataWrapperCommon<dv::Frame> {
protected:
	/**
	 * Resizes the output buffer to the set size and format
	 */
	void resizeToDimensions() {
		dv::Cfg::Node iNode = dvModuleOutputGetInfoNode(moduleData, name.c_str());

		if (ptr->sizeX <= 0) {
			ptr->sizeX = static_cast<int16_t>(iNode.getInt("sizeX"));
		}

		if (ptr->sizeY <= 0) {
			ptr->sizeY = static_cast<int16_t>(iNode.getInt("sizeY"));
		}

		auto channels     = (static_cast<int>(ptr->format) / 8) + 1;
		auto requiredSize = static_cast<size_t>(ptr->sizeX * ptr->sizeY * channels);

		if (ptr->pixels.size() < requiredSize) {
			// In case the buffer does not correspond to the specified sizes, we resize the buffer
			ptr->pixels.resize(requiredSize);
		}
	}

public:
	OutputDataWrapper(NativeType *p, dvModuleData m, const std::string &n) :
		_OutputDataWrapperCommon<dv::Frame>(p, m, n) {
	}

	void commit() noexcept {
		// Ignore frames with no pixels.
		if ((ptr == nullptr) || ptr->pixels.empty()) {
			return;
		}

		// base commit
		_OutputDataWrapperCommon<dv::Frame>::commit();
	}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	/**
	 * Return an OpenCV Mat representing this frame.
	 * Please note the actual backing memory comes from the Frame->pixels vector.
	 * If the underlying vector changes, due to resize or commit, this Mat becomes
	 * invalid and should not be used anymore.
	 * Also, for this to work, the pixels array must already have its memory allocated
	 * such that Frame.pixels.size() >= (Frame.sizeX * Frame.sizeY * Frame.numChannels).
	 * If that is not the case, a std::out_of_range exception is thrown.
	 *
	 * @return an OpenCV Mat
	 */
	cv::Mat getMat() {
		// Verify size.
		resizeToDimensions();

		return (cv::Mat{ptr->sizeY, ptr->sizeX, static_cast<int>(ptr->format), ptr->pixels.data()});
	}

	void commitMat(const cv::Mat &mat) {
		ptr->sizeX = static_cast<int16_t>(mat.cols);
		ptr->sizeY = static_cast<int16_t>(mat.rows);

		switch (mat.channels()) {
			case 1:
				ptr->format = dv::FrameFormat::GRAY;
				break;

			case 3:
				ptr->format = dv::FrameFormat::BGR;
				break;

			case 4:
				ptr->format = dv::FrameFormat::BGRA;
				break;

			default:
				throw std::out_of_range(
					(boost::format("Unsupported number of channels in OpenCV Mat: %d") % mat.channels()).str());
		}

		cv::Mat outMat = getMat();

		switch (mat.depth()) {
			case CV_8U:
				mat.copyTo(outMat);
				break;

			case CV_8S:
				mat.convertTo(outMat, CV_8U, 1, std::abs(std::numeric_limits<int8_t>::min()));
				break;

			case CV_16U:
				mat.convertTo(outMat, CV_8U, 1.0 / 256.0, 0);
				break;

			case CV_16S:
				mat.convertTo(outMat, CV_8U, 1.0 / 256.0, std::abs(std::numeric_limits<int16_t>::min()));
				break;

			case CV_32S:
				mat.convertTo(outMat, CV_8U, 1.0 / 16777216.0, std::abs(std::numeric_limits<int32_t>::min()));
				break;

			case CV_32F:
			case CV_64F:
				// Floating point range is 0.0 to 1.0 here.
				mat.convertTo(outMat, CV_8U, 255, 0);
				break;

			default:
				throw std::out_of_range((boost::format("Unsupported OpenCV data type: %d") % mat.depth()).str());
		}

		commit();
	}

	/**
	 * Copies the data from the supplied OpenCV matrix and commits it to the output.
	 * @param mat The matrix to be sent to the output
	 * @return A reference to this output, to send more data
	 */
	OutputDataWrapper<dv::Frame> &operator<<(const cv::Mat &mat) {
		commitMat(mat);
		return *this;
	}
#endif

	/**
	 * Returns the dv frame format of the current frame
	 * @return the dv frame format of the current frame
	 */
	dv::FrameFormat format() const noexcept {
		return ptr->format;
	}

	/**
	 * The width of the current frame
	 * @return the width of the current frame
	 */
	int16_t sizeX() const noexcept {
		return ptr->sizeX;
	}

	/**
	 * The height of the current frame
	 * @return the height of the current frame
	 */
	int16_t sizeY() const noexcept {
		return ptr->sizeY;
	}

	/**
	 * Returns the position in x of the region of interest (ROI) of the current frame
	 * @return the x coordinate of the start of the region of interest
	 */
	int16_t positionX() const noexcept {
		return ptr->positionX;
	}

	/**
	 * Returns the position in y of the region of interest (ROI) of the current frame
	 * @return the y coordinate of the start of the region of interest
	 */
	int16_t positionY() const noexcept {
		return ptr->positionY;
	}

	/**
	 * Access a vector of pixels in this frame
	 * @return A const reference to the vector of pixels in this frame
	 */
	const dv::cvector<uint8_t> &pixels() const noexcept {
		return ptr->pixels;
	}

	/**
	 * Returns the timestamp of this frame
	 * @return The timestamp of this frame
	 */
	int64_t timestamp() const noexcept {
		return ptr->timestamp;
	}

	/**
	 * Returns the timestamp of the start of this frame
	 */
	int64_t timestampStartOfFrame() const noexcept {
		return ptr->timestampStartOfFrame;
	}

	/**
	 * Returns the timestamp of the end of this frame
	 * @return timestamp of the end of this frame
	 */
	int64_t timestampEndOfFrame() const noexcept {
		return ptr->timestampEndOfFrame;
	}

	/**
	 * Returns the timestamp of the start of exposure for this frame
	 * @return the timestamp of the end of exposure for this frame
	 */
	int64_t timestampStartOfExposure() const noexcept {
		return ptr->timestampStartOfExposure;
	}

	/**
	 * Returns the timestamp of the end of exposure for this frame
	 * @return the timestamp of the end of exposure for this frame
	 */
	int64_t timestampEndOfExposure() const noexcept {
		return ptr->timestampEndOfExposure;
	}

	/**
	 * Sets the output frames format to the supplied format
	 */
	void setFormat(dv::FrameFormat format) noexcept {
		ptr->format = format;
		resizeToDimensions();
	}

	/**
	 * Sets the height / width of the output frame
	 */
	void setSize(int16_t sizeX, int16_t sizeY) noexcept {
		ptr->sizeX = sizeX;
		ptr->sizeY = sizeY;
		resizeToDimensions();
	}

	/**
	 * Sets the ROI start position for the output frame
	 */
	void setPosition(int16_t positionX, int16_t positionY) noexcept {
		ptr->positionX = positionX;
		ptr->positionY = positionY;
	}

	/**
	 * Sets the ROI of the output frame
	 */
	void setROI(int16_t positionX, int16_t positionY, int16_t sizeX, int16_t sizeY) noexcept {
		setPosition(positionX, positionY);
		setSize(sizeX, sizeY);
	}

	/**
	 * Access a vector of pixels in this frame
	 * @return A reference to the vector of pixels in this frame
	 */
	dv::cvector<uint8_t> &pixels() noexcept {
		resizeToDimensions();

		return ptr->pixels;
	}

	/**
	 * Sets the timestamp of the output frame
	 */
	void setTimestamp(int64_t timestamp) noexcept {
		ptr->timestamp = timestamp;
	}

	/**
	 * Sets the timestampStartOfFrame of the output frame
	 */
	void setTimestampStartOfFrame(int64_t timestampStartOfFrame) noexcept {
		ptr->timestampStartOfFrame = timestampStartOfFrame;
	}

	/**
	 * Sets the timestampEndOfFrame of the output frame
	 */
	void setTimestampEndOfFrame(int64_t timestampEndOfFrame) noexcept {
		ptr->timestampEndOfFrame = timestampEndOfFrame;
	}

	/**
	 * Sets the timestampStartOfExposure of the output frame
	 */
	void setTimestampStartOfExposure(int64_t timestampStartOfExposure) noexcept {
		ptr->timestampStartOfExposure = timestampStartOfExposure;
	}

	/**
	 * Sets the timestamp of the output frame
	 */
	void setTimestampEndOfExposure(int64_t timestampEndOfExposure) noexcept {
		ptr->timestampEndOfExposure = timestampEndOfExposure;
	}

	/**
	 * Sets the timestamp of the current frame on the output.
	 * @param timestamp The timestamp to be set
	 * @return A reference to this data, to commit more data
	 */
	OutputDataWrapper<dv::Frame> &operator<<(int64_t timestamp) {
		setTimestamp(timestamp);
		return *this;
	}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	/**
	 * Returns the size of the current frame. This is generally the same as the input
	 * dimension, but depending on the ROI setting, can be less
	 * @return The size of the current frame
	 */
	cv::Size size() const {
		return cv::Size(sizeX(), sizeY());
	}

	/**
	 * Returns the position of the ROI of the current frame.
	 * @return The position of the start of the ROI of the current frame
	 */
	cv::Point position() const {
		return cv::Point(positionX(), positionY());
	}

	/**
	 * Returns the rectangle defining the region of interest of the current frame
	 * @return The rectangle describing the region of interest of the current frame
	 */
	cv::Rect roi() const {
		return cv::Rect(position(), size());
	}

	/**
	 * Sets the output frame dimensions from an OpenCV Size
	 */
	void setSize(const cv::Size &size) noexcept {
		setSize(static_cast<int16_t>(size.width), static_cast<int16_t>(size.height));
	}

	/**
	 * Sets the position of the ROI of the output frame.
	 */
	void setPosition(const cv::Point &position) noexcept {
		setPosition(static_cast<int16_t>(position.x), static_cast<int16_t>(position.y));
	}

	/**
	 * Sets the rectangle defining the region of interest of the output frame
	 */
	void setROI(const cv::Rect &roi) noexcept {
		setPosition(roi.tl());
		setSize(roi.size());
	}
#endif
};

template<> class RuntimeInput<dv::Frame> : public _RuntimeInputCommon<dv::Frame> {
public:
	RuntimeInput(const std::string &name, dvModuleData moduleData) : _RuntimeInputCommon<dv::Frame>(name, moduleData) {
	}

	const InputDataWrapper<dv::Frame> frame() const {
		return (data());
	}

	int sizeX() const {
		return (infoNode().getInt("sizeX"));
	}

	int sizeY() const {
		return (infoNode().getInt("sizeY"));
	}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	const cv::Size size() const {
		return (cv::Size(sizeX(), sizeY()));
	}
#endif
};

/**
 * Specialization of the runtime output for frame outputs
 * Provides convenience setup functions for setting up the frame output
 */
template<> class RuntimeOutput<dv::Frame> : public _RuntimeOutputCommon<dv::Frame> {
public:
	RuntimeOutput(const std::string &name, dvModuleData moduleData) :
		_RuntimeOutputCommon<dv::Frame>(name, moduleData) {
	}

	/**
	 * Returns the current output frame to set up values and to get the back data buffer
	 * @return The output frame to set up
	 */
	OutputDataWrapper<dv::Frame> frame() {
		return (data());
	}

	/**
	 * Sets up this frame output with the provided parameters
	 * @param sizeX The width of the frames supplied on this output
	 * @param sizeY The height of the frames supplied on this output
	 * @param originDescription A description of the original creator of the data
	 */
	void setup(int sizeX, int sizeY, const std::string &originDescription) {
		createSourceAttribute(originDescription);
		createSizeAttributes(sizeX, sizeY);
	}

	/**
	 * Sets this frame output up with the same parameters the the supplied input.
	 * @param input An input to copy the data from
	 */
	template<typename U> void setup(const RuntimeInput<U> &input) {
		input.infoNode().copyTo(infoNode());

		// Check that required attributes exist at least.
		getOriginDescription();
		sizeX();
		sizeY();
	}

	/**
	 * Sets this frame output up with the same parameters as the supplied vector input.
	 * @param input A vector input to copy the information from
	 */
	template<typename U, typename TT> void setup(const RuntimeVectorInput<U, TT> &input) {
		input.infoNode().copyTo(infoNode());

		// Check that required attributes exist at least.
		getOriginDescription();
		sizeX();
		sizeY();
	}

	int sizeX() const {
		return (infoNode().getInt("sizeX"));
	}

	int sizeY() const {
		return (infoNode().getInt("sizeY"));
	}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
	const cv::Size size() const {
		return (cv::Size(sizeX(), sizeY()));
	}

	/**
	 * Sets the timestamp of the current frame on the output.
	 * @param timestamp The timestamp to be set
	 * @return A reference to this output, to commit more data
	 */
	RuntimeOutput<dv::Frame> &operator<<(int64_t timestamp) {
		data() << timestamp;
		return *this;
	}

	/**
	 * Convenience shorthand to commit an OpenCV mat onto this output.
	 * If not using this function, call `data()` to get an output frame
	 * to fill into.
	 * @param mat The OpenCV Mat to submit
	 * @return A reference to the this
	 */
	RuntimeOutput<dv::Frame> &operator<<(const cv::Mat &mat) {
		data() << mat;
		return *this;
	}

#endif
};

} // namespace dv

#endif // DV_SDK_FRAME_HPP
