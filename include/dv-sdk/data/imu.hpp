#ifndef DV_SDK_IMU_HPP
#define DV_SDK_IMU_HPP

#include "imu_base.hpp"
#include "wrappers.hpp"

namespace dv {

template<>
class InputVectorDataWrapper<dv::IMUPacket, dv::IMUT> : public _InputVectorDataWrapperCommon<dv::IMUPacket, dv::IMUT> {
public:
	InputVectorDataWrapper(std::shared_ptr<const NativeType> p) :
		_InputVectorDataWrapperCommon<dv::IMUPacket, dv::IMUT>(std::move(p)) {
	}
};

template<>
class OutputVectorDataWrapper<dv::IMUPacket, dv::IMUT>
	: public _OutputVectorDataWrapperCommon<dv::IMUPacket, dv::IMUT> {
public:
	OutputVectorDataWrapper(NativeType *p, dvModuleData m, const std::string &n) :
		_OutputVectorDataWrapperCommon<dv::IMUPacket, dv::IMUT>(p, m, n) {
	}

	// Un-hide copy assignment.
	using _OutputDataWrapperCommon<dv::IMUPacket>::operator=;
};

} // namespace dv

#endif // DV_SDK_IMU_HPP
