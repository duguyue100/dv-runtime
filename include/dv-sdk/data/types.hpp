#ifndef DV_SDK_TYPES_HPP
#define DV_SDK_TYPES_HPP

#include "../utils.h"

#ifdef __cplusplus
constexpr static uint32_t dvTypeIdentifierToId(const char *x) {
	uint32_t ret = static_cast<uint8_t>(x[3]);
	ret |= static_cast<uint32_t>(static_cast<uint8_t>(x[2]) << 8);
	ret |= static_cast<uint32_t>(static_cast<uint8_t>(x[1]) << 16);
	ret |= static_cast<uint32_t>(static_cast<uint8_t>(x[0]) << 24);
	return (ret);
}

extern "C" {
#endif

struct dvTypeTimeElementExtractor {
	int64_t startTimestamp;
	int64_t endTimestamp;
	int64_t numElements;
};

typedef uint32_t (*dvTypePackFuncPtr)(void *toFlatBufferBuilder, const void *fromObject);
typedef void (*dvTypeUnpackFuncPtr)(void *toObject, const void *fromFlatBuffer);
typedef void *(*dvTypeConstructPtr)(size_t sizeOfObject);
typedef void (*dvTypeDestructPtr)(void *object);
typedef struct dvTypeTimeElementExtractor (*dvTypeTimeElementExtractorPtr)(void *object);
typedef bool (*dvTypeUnpackTimeElementRangeFuncPtr)(
	void *toObject, const void *fromFlatBuffer, struct dvTypeTimeElementExtractor range);

struct dvType {
	uint32_t id;
	const char *identifier;
	const char *description;
	size_t sizeOfType;
	dvTypePackFuncPtr pack;
	dvTypeUnpackFuncPtr unpack;
	dvTypeConstructPtr construct;
	dvTypeDestructPtr destruct;
	dvTypeTimeElementExtractorPtr timeElementExtractor;
	dvTypeUnpackTimeElementRangeFuncPtr unpackTimeElementRange;

#ifdef __cplusplus
	constexpr dvType() :
		id(dvTypeIdentifierToId("NULL")),
		identifier("NULL"),
		description("Placeholder for errors."),
		sizeOfType(0),
		pack(nullptr),
		unpack(nullptr),
		construct(nullptr),
		destruct(nullptr),
		timeElementExtractor(nullptr),
		unpackTimeElementRange(nullptr) {
	}

	constexpr dvType(const char *_identifier, const char *_description, size_t _sizeOfType, dvTypePackFuncPtr _pack,
		dvTypeUnpackFuncPtr _unpack, dvTypeConstructPtr _construct, dvTypeDestructPtr _destruct,
		dvTypeTimeElementExtractorPtr _timeElementExtractor,
		dvTypeUnpackTimeElementRangeFuncPtr _unpackTimeElementRange) :
		id(dvTypeIdentifierToId(_identifier)),
		identifier(_identifier),
		description(_description),
		sizeOfType(_sizeOfType),
		pack(_pack),
		unpack(_unpack),
		construct(_construct),
		destruct(_destruct),
		timeElementExtractor(_timeElementExtractor),
		unpackTimeElementRange(_unpackTimeElementRange) {
		if (identifier == nullptr) {
			throw std::invalid_argument("Type identifier must be defined.");
		}

		if ((identifier[0] == 0) || (identifier[1] == 0) || (identifier[2] == 0) || (identifier[3] == 0)
			|| (identifier[4] != 0)) {
			throw std::invalid_argument("Type identifier must be exactly four characters long.");
		}

		if ((description == nullptr) || (description[0] == 0)) {
			throw std::invalid_argument("Type description must be defined.");
		}
	}

	bool operator==(const dvType &rhs) const noexcept {
		return ((id == rhs.id) && (sizeOfType == rhs.sizeOfType) && (pack == rhs.pack) && (unpack == rhs.unpack)
				&& (construct == rhs.construct) && (destruct == rhs.destruct)
				&& (timeElementExtractor == rhs.timeElementExtractor)
				&& (unpackTimeElementRange == rhs.unpackTimeElementRange));
	}

	bool operator!=(const dvType &rhs) const noexcept {
		return (!operator==(rhs));
	}
#endif
};

LIB_PUBLIC_VISIBILITY struct dvType dvTypeSystemGetInfoByIdentifier(const char *tIdentifier);
LIB_PUBLIC_VISIBILITY struct dvType dvTypeSystemGetInfoByID(uint32_t tId);

struct dvTypedObject {
	uint32_t typeId;
	size_t objSize;
	void *obj;

#ifdef __cplusplus
	dvTypedObject(const dvType &t) {
		typeId  = t.id;
		objSize = t.sizeOfType;
		obj     = (*t.construct)(objSize);

		if (obj == nullptr) {
			throw std::bad_alloc();
		}
	}

	~dvTypedObject() noexcept {
		const auto t = dvTypeSystemGetInfoByID(typeId);
		(*t.destruct)(obj);
	}

	bool operator==(const dvTypedObject &rhs) const noexcept {
		return ((typeId == rhs.typeId) && (objSize == rhs.objSize) && (obj == rhs.obj));
	}

	bool operator!=(const dvTypedObject &rhs) const noexcept {
		return (!operator==(rhs));
	}
#endif
};

#ifdef __cplusplus
}

static_assert(std::is_standard_layout_v<dvType>, "dvType is not standard layout");
static_assert(std::is_standard_layout_v<dvTypedObject>, "dvTypedObject is not standard layout");

#	include "flatbuffers/flatbuffers.h"
#	include "cvector.hpp"
#	include <boost/tti/has_member_data.hpp>
#	include <boost/tti/has_member_function.hpp>

namespace dv::Types {

constexpr static const char *nullIdentifier = "NULL";
constexpr static const uint32_t nullId      = dvTypeIdentifierToId(nullIdentifier);

constexpr static const char *anyIdentifier = "ANYT";
constexpr static const uint32_t anyId      = dvTypeIdentifierToId(anyIdentifier);

using Type        = dvType;
using TypedObject = dvTypedObject;

using PackFuncPtr                   = dvTypePackFuncPtr;
using UnpackFuncPtr                 = dvTypeUnpackFuncPtr;
using ConstructPtr                  = dvTypeConstructPtr;
using DestructPtr                   = dvTypeDestructPtr;
using TimeElementExtractorPtr       = dvTypeTimeElementExtractorPtr;
using UnpackTimeElementRangeFuncPtr = dvTypeUnpackTimeElementRangeFuncPtr;

template<typename FBType> constexpr static uint32_t Packer(void *toFlatBufferBuilder, const void *fromObject) {
	using ObjectAPIType = typename FBType::NativeTableType;

	assert(toFlatBufferBuilder != nullptr);
	assert(fromObject != nullptr);

	return (FBType::Pack(*(static_cast<flatbuffers::FlatBufferBuilder *>(toFlatBufferBuilder)),
		static_cast<const ObjectAPIType *>(fromObject), nullptr)
				.o);
}

template<typename FBType> constexpr static void Unpacker(void *toObject, const void *fromFlatBuffer) {
	using ObjectAPIType = typename FBType::NativeTableType;

	assert(toObject != nullptr);
	assert(fromFlatBuffer != nullptr);

	FBType::UnPackToFrom(static_cast<ObjectAPIType *>(toObject), static_cast<const FBType *>(fromFlatBuffer), nullptr);
}

template<typename FBType> constexpr static void *Constructor(size_t sizeOfObject) {
	using ObjectAPIType = typename FBType::NativeTableType;

	auto obj = static_cast<ObjectAPIType *>(malloc(sizeOfObject));
	if (obj == nullptr) {
		throw std::bad_alloc();
	}

	new (obj) ObjectAPIType{};

	return (obj);
}

template<typename FBType> constexpr static void Destructor(void *object) {
	using ObjectAPIType = typename FBType::NativeTableType;

	assert(object != nullptr);

	auto obj = static_cast<ObjectAPIType *>(object);

	std::destroy_at(obj);

	free(obj);
}

BOOST_TTI_HAS_MEMBER_DATA(elements)
template<typename T, typename U>
inline constexpr bool object_has_elements = has_member_data_elements<T, dv::cvector<U>>::value;

BOOST_TTI_HAS_MEMBER_FUNCTION(elements)
template<typename T, typename U>
inline constexpr bool flatbuffer_has_elements_with_struct
	= has_member_function_elements<const flatbuffers::Vector<const U *> *(T::*) () const>::value;
template<typename T, typename U>
inline constexpr bool flatbuffer_has_elements_with_table
	= has_member_function_elements<const flatbuffers::Vector<flatbuffers::Offset<U>> *(T::*) () const>::value;

// Flatbuffer tables.
BOOST_TTI_HAS_MEMBER_DATA(timestamp)
template<typename T> inline constexpr bool has_timestamp_as_data = has_member_data_timestamp<T, int64_t>::value;

// Flatbuffer structs.
BOOST_TTI_HAS_MEMBER_FUNCTION(timestamp)
template<typename T>
inline constexpr bool has_timestamp_as_function = has_member_function_timestamp<int64_t (T::*)() const>::value;

template<typename ObjectAPIType, typename SubObjectAPIType>
constexpr static struct dvTypeTimeElementExtractor TimeElementExtractor(void *object) {
	assert(object != nullptr);

	auto obj = static_cast<ObjectAPIType *>(object);

	// Information extraction mode: get number of contained elements
	// and timestamp related information.
	struct dvTypeTimeElementExtractor result {
		- 1, -1, -1
	};

	if constexpr (object_has_elements<ObjectAPIType, SubObjectAPIType>) {
		result.numElements = obj->elements.size();

		if constexpr (has_timestamp_as_data<SubObjectAPIType>) {
			result.startTimestamp = obj->elements.front().timestamp;
			result.endTimestamp   = obj->elements.back().timestamp;
		}
		else if constexpr (has_timestamp_as_function<SubObjectAPIType>) {
			result.startTimestamp = obj->elements.front().timestamp();
			result.endTimestamp   = obj->elements.back().timestamp();
		}
		else {
			result.startTimestamp = result.endTimestamp = -1;
		}
	}
	else {
		result.numElements = 1;

		if constexpr (has_timestamp_as_data<ObjectAPIType>) {
			result.startTimestamp = result.endTimestamp = obj->timestamp;
		}
		else if constexpr (has_timestamp_as_function<ObjectAPIType>) {
			result.startTimestamp = result.endTimestamp = obj->timestamp();
		}
		else {
			result.startTimestamp = result.endTimestamp = -1;
		}
	}

	return (result);
}

template<typename FBType, typename SubObjectFBType>
constexpr static bool UnpackTimeElementRange(
	void *toObject, const void *fromFlatBuffer, struct dvTypeTimeElementExtractor range) {
	using ObjectAPIType = typename FBType::NativeTableType;

	assert(toObject != nullptr);
	assert(fromFlatBuffer != nullptr);

	auto toObj  = static_cast<ObjectAPIType *>(toObject);
	auto fromFB = static_cast<const FBType *>(fromFlatBuffer);

	if (range.numElements == -1) {
		// Do time-based range extraction, if timestamp exists.
		// Operate directly on Flatbuffers.
		if constexpr (flatbuffer_has_elements_with_table<FBType, SubObjectFBType>) {
			if constexpr (has_timestamp_as_function<SubObjectFBType>) {
				auto _e = fromFB->elements();
				if (_e) {
					auto _j = toObj->elements.size();
					toObj->elements.resize(_j + _e->size());

					for (flatbuffers::uoffset_t _i = 0; _i < _e->size(); _i++) {
						auto elem = _e->Get(_i);
						if (elem->timestamp() >= range.startTimestamp && elem->timestamp() <= range.endTimestamp) {
							elem->UnPackTo(&toObj->elements[_j++], nullptr);
						}
					}

					toObj->elements.resize(_j);
				}
			}
		}
		else if constexpr (flatbuffer_has_elements_with_struct<FBType, SubObjectFBType>) {
			if constexpr (has_timestamp_as_function<SubObjectFBType>) {
				auto _e = fromFB->elements();
				if (_e) {
					auto _j = toObj->elements.size();
					toObj->elements.resize(_j + _e->size());

					for (flatbuffers::uoffset_t _i = 0; _i < _e->size(); _i++) {
						auto elem = _e->Get(_i);
						if (elem->timestamp() >= range.startTimestamp && elem->timestamp() <= range.endTimestamp) {
							toObj->elements[_j++] = *elem;
						}
					}

					toObj->elements.resize(_j);
				}
			}
		}
		else {
			if constexpr (has_timestamp_as_function<FBType>) {
				if (fromFB->timestamp() >= range.startTimestamp && fromFB->timestamp() <= range.endTimestamp) {
					Unpacker<FBType>(toObj, fromFB);
					return (true);
				}
			}
		}
	}
	else {
		// Element count-based extraction.
		// TODO: implement later.
	}

	// No commit.
	return (false);
}

template<typename FBType, typename SubObjectFBType> constexpr static Type makeTypeDefinition(const char *description) {
	using ObjectAPIType    = typename FBType::NativeTableType;
	using SubObjectAPIType = typename SubObjectFBType::NativeTableType;

	static_assert(std::is_standard_layout_v<ObjectAPIType>, "ObjectAPIType is not standard layout");
	static_assert(std::is_standard_layout_v<SubObjectAPIType>, "SubObjectAPIType is not standard layout");

	return (Type{FBType::identifier, description, sizeof(ObjectAPIType), &Packer<FBType>, &Unpacker<FBType>,
		&Constructor<FBType>, &Destructor<FBType>, &TimeElementExtractor<ObjectAPIType, SubObjectAPIType>,
		&UnpackTimeElementRange<FBType, SubObjectFBType>});
}

} // namespace dv::Types

#endif

#endif // DV_SDK_TYPES_HPP
