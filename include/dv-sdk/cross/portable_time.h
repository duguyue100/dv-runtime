#ifndef DV_SDK_PORTABLE_TIME_H_
#define DV_SDK_PORTABLE_TIME_H_

#ifdef __cplusplus

#	include <cstdlib>
#	include <ctime>

#else

#	include <stdbool.h>
#	include <stdlib.h>
#	include <time.h>

#endif

#if defined(_WIN32) || defined(__CYGWIN__)
#	ifdef __GNUC__
#		define LIB_PUBLIC_VISIBILITY __attribute__((dllexport))
#	else
#		define LIB_PUBLIC_VISIBILITY __declspec(dllexport)
#	endif
#else
#	define LIB_PUBLIC_VISIBILITY __attribute__((visibility("default")))
#endif

#ifdef __cplusplus
extern "C" {
#endif

LIB_PUBLIC_VISIBILITY bool portable_clock_gettime_monotonic(struct timespec *monoTime);
LIB_PUBLIC_VISIBILITY bool portable_clock_gettime_realtime(struct timespec *realTime);
LIB_PUBLIC_VISIBILITY struct tm portable_clock_localtime(void);

#ifdef __cplusplus
}
#endif

#endif /* DV_SDK_PORTABLE_TIME_H_ */
