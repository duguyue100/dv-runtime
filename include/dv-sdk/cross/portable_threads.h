#ifndef DV_SDK_PORTABLE_THREADS_H_
#define DV_SDK_PORTABLE_THREADS_H_

#ifdef __cplusplus

#	include <cstdlib>

#else

#	include <stdbool.h>
#	include <stdlib.h>

#endif

#if defined(_WIN32) || defined(__CYGWIN__)
#	ifdef __GNUC__
#		define LIB_PUBLIC_VISIBILITY __attribute__((dllexport))
#	else
#		define LIB_PUBLIC_VISIBILITY __declspec(dllexport)
#	endif
#else
#	define LIB_PUBLIC_VISIBILITY __attribute__((visibility("default")))
#endif

#ifdef __cplusplus
extern "C" {
#endif

LIB_PUBLIC_VISIBILITY bool portable_thread_set_name(const char *name);
LIB_PUBLIC_VISIBILITY bool portable_thread_set_priority_highest(void);

#ifdef __cplusplus
}
#endif

#endif /* DV_SDK_PORTABLE_THREADS_H_ */
