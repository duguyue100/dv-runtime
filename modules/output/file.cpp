#include "dv-sdk/cross/portable_io.h"
#include "dv-sdk/cross/portable_time.h"

#include "dv_output.hpp"

#include <boost/filesystem.hpp>
#include <ctime>
#include <fcntl.h>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>

class OutFile : public dv::ModuleBase {
private:
	struct fileHeader {
		dv::IOHeaderT data;
		size_t offset;
		size_t length;
	};

	struct fileOutput {
		std::ofstream stream;
		fileHeader header;
	};

	fileOutput fileStream;

	dv::OutputEncoder output;

public:
	static void addInputs(dv::InputDefinitionList &in) {
		in.addInput("output0", "ANYT", false);
		in.addInput("output1", "ANYT", true);
		in.addInput("output2", "ANYT", true);
		in.addInput("output3", "ANYT", true);
	}

	static const char *getDescription() {
		return ("Write AEDAT 4.0 data to a file.");
	}

	static void getConfigOptions(dv::RuntimeConfig &config) {
		getConfigOptionsOutputCommon(config);

		auto homeDir = portable_get_user_home_directory();
		config.add("directory", dv::ConfigOption::directoryOption("Directory in which to save data.", homeDir));
		free(homeDir);

		config.add("prefix", dv::ConfigOption::stringOption(
								 "String to add in front of the date part of the final save name.", "dvSave"));

		config.setPriorityOptions({"directory", "prefix", "compression"});
	}

	static void advancedStaticInit(dvModuleData mData) {
		// TODO: flexible number of outputs.

		dv::Cfg::Node(mData->moduleNode)
			.create<dv::CfgType::STRING>("file", "", {0, INT32_MAX}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT,
				"Full name of file being currently written.");
	}

	static void writeHeader(fileOutput &file) {
		// Construct serialized flatbuffer header.
		flatbuffers::FlatBufferBuilder headerBuild{2 * 1024};

		headerBuild.ForceDefaults(true);

		auto headerOffset = dv::IOHeader::Pack(headerBuild, &file.header.data);

		dv::FinishSizePrefixedIOHeaderBuffer(headerBuild, headerOffset);

		uint8_t *headerData   = headerBuild.GetBufferPointer();
		size_t headerDataSize = headerBuild.GetSize();

		if (file.header.offset == 0) {
			// First header write, store offset and length.
			file.header.offset = static_cast<size_t>(file.stream.tellp());
			file.header.length = headerDataSize;

			assert(file.header.offset
				   == static_cast<std::underlying_type_t<dv::Constants>>(dv::Constants::AEDAT_VERSION_LENGTH));
			assert(file.header.length > 14);
		}
		else {
			// Final header write, use offset and length.
			assert(headerDataSize == file.header.length);
		}

		file.stream.seekp(static_cast<std::streamoff>(file.header.offset));
		file.stream.write(reinterpret_cast<char *>(headerData), static_cast<std::streamsize>(headerDataSize));

		file.stream.flush();
	}

	OutFile() : output(&config, &log) {
		// Get current time for suffix part.
		auto currentTime = portable_clock_localtime();

		// Following time format uses exactly 19 characters (5 separators,
		// 4 year, 2 month, 2 day, 2 hours, 2 minutes, 2 seconds).
		size_t currentTimeStringLength = 19;
		char currentTimeString[currentTimeStringLength + 1]; // + 1 for terminating NUL byte.
		strftime(currentTimeString, currentTimeStringLength + 1, "%Y_%m_%d_%H_%M_%S", &currentTime);

		// Determine new save name.
		auto saveName
			= config.get<dv::CfgType::STRING>("prefix") + "-" + currentTimeString + ("." AEDAT4_FILE_EXTENSION);

		// Create directory.
		auto saveDir = boost::filesystem::path(config.get<dv::CfgType::STRING>("directory"));
		boost::filesystem::create_directories(saveDir);

		// Add compression information at startup, won't change during execution.
		auto compressionType = config.get<dv::CfgType::STRING>("compression");

		output.setCompressionType(dv::parseCompressionTypeFromString(compressionType));
		output.setTrackDataTable(true);

		// Setup output node and convert it to string for file output.
		dv::OutputEncoder::makeOutputNode(
			inputs.infoNode("output0"), moduleNode.getRelativeNode("outInfo/0/"), compressionType);

		if (inputs.isConnected("output1")) {
			dv::OutputEncoder::makeOutputNode(
				inputs.infoNode("output1"), moduleNode.getRelativeNode("outInfo/1/"), compressionType);
		}

		if (inputs.isConnected("output2")) {
			dv::OutputEncoder::makeOutputNode(
				inputs.infoNode("output2"), moduleNode.getRelativeNode("outInfo/2/"), compressionType);
		}

		if (inputs.isConnected("output3")) {
			dv::OutputEncoder::makeOutputNode(
				inputs.infoNode("output3"), moduleNode.getRelativeNode("outInfo/3/"), compressionType);
		}

		auto outputNodeString = moduleNode.getRelativeNode("outInfo/").exportSubTreeToXMLString(true);

		// Open file for writing.
		auto fileName     = (saveDir / saveName).string();
		fileStream.stream = std::ofstream(fileName, std::ios::out | std::ios::binary);

		moduleNode.updateReadOnly<dv::CfgType::STRING>("file", fileName);

		// Write AEDAT 4.0 header.
		fileStream.stream.write("#!AER-DAT4.0\r\n",
			static_cast<std::underlying_type_t<dv::Constants>>(dv::Constants::AEDAT_VERSION_LENGTH));

		fileStream.header.data.infoNode          = outputNodeString;
		fileStream.header.data.compression       = dv::parseCompressionTypeFromString(compressionType);
		fileStream.header.data.dataTablePosition = -1; // Data jump table not present yet.

		writeHeader(fileStream);

		log.info.format("Writing data to file '%s'.", fileName);
	}

	~OutFile() override {
		// Ensure data written out until now is committed.
		fileStream.stream.flush();

		// Cleanup manually added output info nodes.
		moduleNode.getRelativeNode("outInfo/").removeNode();

		// Write out file data table and update header.
		auto fileDataTable = output.getTrackDataTable();

		fileStream.header.data.dataTablePosition = fileStream.stream.tellp();

		fileStream.stream.write(fileDataTable->getData(), static_cast<std::streamsize>(fileDataTable->getSize()));

		writeHeader(fileStream);
	}

	void writePacket(const char *outputName, int32_t streamId) {
		auto input = dvModuleInputGet(moduleData, outputName);

		if (input != nullptr) {
			// outData.size is in bytes, not elements.
			auto outMessage = output.processPacket(input);

			// Write packet header first.
			const dv::PacketHeader pktHeader(streamId, static_cast<int32_t>(outMessage->getSize()));
			fileStream.stream.write(reinterpret_cast<const char *>(&pktHeader), sizeof(dv::PacketHeader));

			// Then write packet content.
			auto dataPosition = fileStream.stream.tellp();
			fileStream.stream.write(outMessage->getData(), static_cast<std::streamsize>(outMessage->getSize()));

			// Update file data table with a new entry.
			output.updateTrackDataTable(dataPosition, pktHeader, input);

			// Dismiss data, freeing it.
			dvModuleInputDismiss(moduleData, outputName, input);
		}
	}

	void run() override {
		writePacket("output0", 0);

		if (inputs.isConnected("output1")) {
			writePacket("output1", 1);
		}

		if (inputs.isConnected("output2")) {
			writePacket("output2", 2);
		}

		if (inputs.isConnected("output3")) {
			writePacket("output3", 3);
		}
	}
};

registerModuleClass(OutFile)
