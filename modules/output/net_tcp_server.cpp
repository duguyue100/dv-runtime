#include "dv-sdk/cross/asio_tcptlssocket.hpp"

#include "dv_output.hpp"

class NetTCPServer;

class Connection : public std::enable_shared_from_this<Connection> {
private:
	NetTCPServer *parent;
	TCPTLSWriteOrderedSocket socket;
	uint8_t keepAliveReadSpace;

public:
	Connection(asioTCP::socket s, bool tlsEnabled, asioSSL::context *tlsContext, NetTCPServer *server);
	~Connection();

	void start();
	void close();
	void writeMessage(std::shared_ptr<const dv::OutputData> message);

private:
	void keepAliveByReading();
	void handleError(const boost::system::error_code &error, const char *message);
};

class NetTCPServer : public dv::ModuleBase {
private:
	asio::io_service ioService;
	asioTCP::acceptor acceptor;
	asioTCP::socket acceptorNewSocket;
	asioSSL::context tlsContext;
	bool tlsEnabled;

	std::vector<Connection *> clients;
	dv::OutputEncoder output;

	bool portAutoSelect;

public:
	static void addInputs(dv::InputDefinitionList &in) {
		in.addInput("output0", dv::Types::anyIdentifier, false);
	}

	static const char *getDescription() {
		return ("Send AEDAT 4 data out via TCP to connected clients (server mode).");
	}

	static void getConfigOptions(dv::RuntimeConfig &config) {
		getConfigOptionsOutputCommon(config);

		// Determine default IP address based on ConfigServer IP configuration.
		std::string ipAddress = "127.0.0.1";
		if (dv::Config::GLOBAL.getNode("/system/server/").getString("ipAddress") == "0.0.0.0") {
			ipAddress = "0.0.0.0";
		}

		config.add("ipAddress", dv::ConfigOption::stringOption("IPv4 address to listen on (server mode).", ipAddress));
		config.add(
			"portNumber", dv::ConfigOption::intOption("Port number to listen on (server mode).", 7777, 0, UINT16_MAX));
		config.add("backlogSize", dv::ConfigOption::intOption("Maximum number of pending connections.", 5, 1, 32));
		config.add("concurrentConnections",
			dv::ConfigOption::intOption("Maximum number of concurrent active connections.", 10, 1, 128));

		config.setPriorityOptions({"ipAddress", "portNumber", "compression"});
	}

	NetTCPServer() :
		acceptor(ioService),
		acceptorNewSocket(ioService),
		tlsContext(asioSSL::context::tlsv12_server),
		tlsEnabled(false),
		output(&config, &log),
		portAutoSelect(false) {
		// Add compression information at startup, won't change during execution.
		auto compressionType = config.get<dv::CfgType::STRING>("compression");

		output.setCompressionType(dv::parseCompressionTypeFromString(compressionType));

		// Required input is always present.
		dv::OutputEncoder::makeOutputNode(
			inputs.infoNode("output0"), moduleNode.getRelativeNode("outInfo/0/"), compressionType);

		// Configure acceptor.
		auto endpoint = asioTCP::endpoint(asioIP::address::from_string(config.get<dv::CfgType::STRING>("ipAddress")),
			static_cast<uint16_t>(config.get<dv::CfgType::INT>("portNumber")));

		try {
			acceptor.open(endpoint.protocol());
			acceptor.set_option(asioTCP::socket::reuse_address(true));
			acceptor.bind(endpoint);
			acceptor.listen();
		}
		catch (const std::exception &) {
			acceptor.close();
			moduleNode.getRelativeNode("outInfo/").removeNode();
			throw;
		}

		// If port was zero, we want to update with the actual port number.
		if (config.get<dv::CfgType::INT>("portNumber") == 0) {
			config.set<dv::CfgType::INT>("portNumber", acceptor.local_endpoint().port());

			portAutoSelect = true;
		}

		acceptStart();

		log.info.format("Output server ready on %s:%d.", config.get<dv::CfgType::STRING>("ipAddress"),
			config.get<dv::CfgType::INT>("portNumber"));
	}

	~NetTCPServer() override {
		acceptor.close();

		// Post 'close all connections' to end of async queue,
		// so that any other callbacks, such as pending accepts,
		// are executed first, and we really close all sockets.
		ioService.post([this]() {
			// Close all open connections, hard.
			for (const auto client : clients) {
				client->close();
			}
		});

		// Wait for all clients to go away.
		while (!clients.empty()) {
			ioService.poll();
#if defined(BOOST_VERSION) && (BOOST_VERSION / 100000) == 1 && (BOOST_VERSION / 100 % 1000) >= 66
			ioService.restart();
#else
			ioService.reset();
#endif
		}

		// Cleanup manually added output info nodes.
		moduleNode.getRelativeNode("outInfo/").removeNode();

		// Reset port to 0 for auto-select.
		if (portAutoSelect) {
			config.set<dv::CfgType::INT>("portNumber", 0);
		}
	}

	void removeClient(Connection *client) {
		dv::vectorRemove(clients, client);
	}

	void run() override {
		auto input0 = dvModuleInputGet(moduleData, "output0");

		if (input0 != nullptr) {
			// outData.size is in bytes, not elements.
			auto outMessage = output.processPacket(input0);

			for (const auto client : clients) {
				client->writeMessage(outMessage);
			}

			dvModuleInputDismiss(moduleData, "output0", input0);
		}

		ioService.poll();
#if defined(BOOST_VERSION) && (BOOST_VERSION / 100000) == 1 && (BOOST_VERSION / 100 % 1000) >= 66
		ioService.restart();
#else
		ioService.reset();
#endif
	}

private:
	void acceptStart() {
		acceptor.async_accept(
			acceptorNewSocket,
			[this](const boost::system::error_code &error) {
				if (error) {
					// Ignore cancel error, normal on shutdown.
					if (error != asio::error::operation_aborted) {
						log.error.format(
							"Failed to accept connection. Error: %s (%d).", error.message(), error.value());
					}
				}
				else {
					auto client
						= std::make_shared<Connection>(std::move(acceptorNewSocket), tlsEnabled, &tlsContext, this);

					clients.push_back(client.get());

					client->start();

					acceptStart();
				}
			},
			nullptr);
	}
};

Connection::Connection(asioTCP::socket s, bool tlsEnabled, asioSSL::context *tlsContext, NetTCPServer *server) :
	parent(server),
	socket(std::move(s), tlsEnabled, tlsContext),
	keepAliveReadSpace(0) {
	parent->log.info.format(
		"New connection from client %s:%d.", socket.remote_address().to_string(), socket.remote_port());
}

Connection::~Connection() {
	parent->removeClient(this);

	parent->log.info.format(
		"Closing connection from client %s:%d.", socket.remote_address().to_string(), socket.remote_port());
}

void Connection::start() {
	auto self(shared_from_this());

	socket.start(
		[this, self](const boost::system::error_code &error) {
			if (error) {
				handleError(error, "Failed startup (TLS handshake)");
			}
			else {
				keepAliveByReading();
			}
		},
		asioSSL::stream_base::server);
}

void Connection::close() {
	socket.close();
}

void Connection::writeMessage(std::shared_ptr<const dv::OutputData> message) {
	auto self(shared_from_this());

	socket.write(asio::buffer(message->getData(), message->getSize()),
		[this, self, message](const boost::system::error_code &error, size_t /*length*/) {
			if (error) {
				handleError(error, "Failed to write message");
			}
		});
}

void Connection::keepAliveByReading() {
	auto self(shared_from_this());

	socket.read(asio::buffer(&keepAliveReadSpace, sizeof(keepAliveReadSpace)),
		[this, self](const boost::system::error_code &error, size_t /*length*/) {
			if (error) {
				handleError(error, "Read keep-alive failure");
			}
			else {
				handleError(error, "Detected illegal incoming data");
			}
		});
}

void Connection::handleError(const boost::system::error_code &error, const char *message) {
	if (error == asio::error::eof || error == asio::error::broken_pipe || error == asio::error::operation_aborted) {
		// Handle EOF/shutdown separately.
		parent->log.info.format(
			"Client %s:%d: connection closed.", socket.remote_address().to_string(), socket.remote_port());
	}
	else {
		parent->log.error.format("Client %s:%d: %s. Error: %s (%d).", socket.remote_address().to_string(),
			socket.remote_port(), message, error.message(), error.value());
	}
}

registerModuleClass(NetTCPServer)
