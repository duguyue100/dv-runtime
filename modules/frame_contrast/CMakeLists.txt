ADD_LIBRARY(frame_contrast SHARED frame_contrast.cpp)

SET_TARGET_PROPERTIES(frame_contrast
	PROPERTIES
	PREFIX "dv_"
)

TARGET_LINK_LIBRARIES(frame_contrast
	PRIVATE
		dvsdk
		${OpenCV_LIBS})

INSTALL(TARGETS frame_contrast DESTINATION ${DV_MODULES_DIR})
