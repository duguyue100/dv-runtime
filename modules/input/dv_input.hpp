#ifndef DV_INPUT_HPP
#define DV_INPUT_HPP

#include "../output/dv_io.hpp"

#include <boost/filesystem.hpp>
#include <fstream>
#include <map>
#include <thread>

namespace dv {

struct InputStream {
	int32_t id;
	std::string name;
	std::string typeIdentifier;
	dv::Types::Type type;
};

struct InputInformation {
	size_t fileSize;
	dv::CompressionType compression;
	int64_t dataTablePosition;
	size_t dataTableSize;
	int64_t timeLowest;
	int64_t timeHighest;
	int64_t timeDifference;
	int64_t timeShift;
	std::vector<InputStream> streams;
	std::unique_ptr<dv::FileDataTableT> dataTable;
};

class InputDecoder {
private:
	/// Input stream to read data from.
	std::istream *inputStream;
	/// Input information from header.
	const InputInformation *inputInfo;
	/// Module data for outputs access.
	dvModuleData moduleData;
	/// Configuration access.
	dv::RuntimeConfig *config;
	/// Logging access.
	dv::Logger *log;
	/// Support LZ4 compression.
	lz4DecompressionSupport compressionLZ4;
	/// Support Zstd compression.
	zstdDecompressionSupport compressionZstd;
	/// Buffer to hold data while decoding.
	std::vector<char> readBuffer;
	std::vector<char> decompressBuffer;

	static std::unique_ptr<dv::FileDataTableT> loadFileDataTable(std::ifstream &fStream, dv::InputInformation &fInfo) {
		// Only proceed if data table is present.
		if (fInfo.dataTablePosition < 0) {
			// Return empty table.
			return (std::unique_ptr<dv::FileDataTableT>(new dv::FileDataTableT{}));
		}

		// Remember offset to go back here after.
		auto initialOffset = fStream.tellg();

		// Read file table data from file.
		std::vector<char> dataTableBuffer;
		dataTableBuffer.resize(fInfo.dataTableSize);

		fStream.seekg(fInfo.dataTablePosition);
		fStream.read(dataTableBuffer.data(), static_cast<int64_t>(fInfo.dataTableSize));

		// Decompress.
		size_t dataTableSize     = dataTableBuffer.size();
		const char *dataTablePtr = dataTableBuffer.data();

		std::vector<char> decompressBuffer;
		decompressBuffer.reserve(dataTableSize);

		if (fInfo.compression != dv::CompressionType::NONE) {
			if (fInfo.compression == dv::CompressionType::LZ4 || fInfo.compression == dv::CompressionType::LZ4_HIGH) {
				auto ctx = dv::InputDecoder::lz4InitDecompressionContext();

				dv::InputDecoder::decompressLZ4(dataTablePtr, dataTableSize, decompressBuffer, ctx.get());
			}

			if (fInfo.compression == dv::CompressionType::ZSTD || fInfo.compression == dv::CompressionType::ZSTD_HIGH) {
				auto ctx = dv::InputDecoder::zstdInitDecompressionContext();

				dv::InputDecoder::decompressZstd(dataTablePtr, dataTableSize, decompressBuffer, ctx.get());
			}

			dataTablePtr  = decompressBuffer.data();
			dataTableSize = decompressBuffer.size();
		}

		// Verify header content is valid.
		flatbuffers::Verifier dataTableVerify(reinterpret_cast<const uint8_t *>(dataTablePtr), dataTableSize);

		if (!dv::VerifySizePrefixedFileDataTableBuffer(dataTableVerify)) {
			throw std::runtime_error("AEDAT4.0: could not verify IOHeader contents.");
		}

		// Unpack table.
		auto dataTableFlatbuffer = dv::GetSizePrefixedFileDataTable(dataTablePtr);
		auto dataTable           = std::unique_ptr<dv::FileDataTableT>(dataTableFlatbuffer->UnPack());

		// Reset file position to initial one.
		fStream.seekg(initialOffset);

		return (dataTable);
	}

public:
	static dv::InputInformation parseHeader(std::ifstream &fStream, dv::Config::Node mNode) {
		// Extract AEDAT version.
		char aedatVersion[static_cast<std::underlying_type_t<dv::Constants>>(dv::Constants::AEDAT_VERSION_LENGTH)];
		fStream.read(
			aedatVersion, static_cast<std::underlying_type_t<dv::Constants>>(dv::Constants::AEDAT_VERSION_LENGTH));

		// Check if file is really AEDAT 4.0.
		if (memcmp(aedatVersion, "#!AER-DAT4.0\r\n",
				static_cast<std::underlying_type_t<dv::Constants>>(dv::Constants::AEDAT_VERSION_LENGTH))
			!= 0) {
			throw std::runtime_error("AEDAT4.0: no valid version line found.");
		}

		dv::InputInformation result;

		// We want to understand what data is in this file,
		// and return actionable elements to our caller.
		// Parse version 4.0 header content.
		// Uses IOHeader flatbuffer. Get its size first.
		flatbuffers::uoffset_t ioHeaderSize;

		fStream.read(reinterpret_cast<char *>(&ioHeaderSize), sizeof(ioHeaderSize));

		// Size is little-endian.
		ioHeaderSize = le32toh(ioHeaderSize);

		// Allocate memory for header and read it.
		auto ioHeader = std::make_unique<uint8_t[]>(ioHeaderSize);

		fStream.read(reinterpret_cast<char *>(ioHeader.get()), ioHeaderSize);

		// Get file size (should always work in binary mode).
		fStream.seekg(0, std::ios::end);
		size_t fileSize = static_cast<size_t>(fStream.tellg());

		// Back to right after the header.
		fStream.seekg(static_cast<std::underlying_type_t<dv::Constants>>(dv::Constants::AEDAT_VERSION_LENGTH)
						  + sizeof(ioHeaderSize) + ioHeaderSize,
			std::ios::beg);

		// Verify header content is valid.
		flatbuffers::Verifier ioHeaderVerify(ioHeader.get(), ioHeaderSize);

		if (!dv::VerifyIOHeaderBuffer(ioHeaderVerify)) {
			throw std::runtime_error("AEDAT4.0: could not verify IOHeader contents.");
		}

		// Parse header into C++ class.
		auto header = dv::UnPackIOHeader(ioHeader.get());

		// Set file-level return information.
		result.fileSize          = fileSize;
		result.compression       = header->compression;
		result.dataTablePosition = header->dataTablePosition;
		result.dataTableSize
			= (header->dataTablePosition < 0) ? (0) : (fileSize - static_cast<size_t>(header->dataTablePosition));

		// Publish file size to tree too.
		if (mNode) {
			mNode.updateReadOnly<dv::CfgType::LONG>("fileSize", static_cast<int64_t>(result.fileSize));
		}

		// Get file data table to determine seek position.
		result.dataTable = loadFileDataTable(fStream, result);

		if (!result.dataTable->Table.empty()) {
			int64_t lowestTimestamp  = INT64_MAX;
			int64_t highestTimestamp = 0;

			for (const auto &inputDef : result.dataTable->Table) {
				// No timestamp is denoted by -1.
				if (inputDef.TimestampStart == -1) {
					continue;
				}

				// Determine lowest and highest timestamps present in file.
				if (inputDef.TimestampStart < lowestTimestamp) {
					lowestTimestamp = inputDef.TimestampStart;
				}

				if (inputDef.TimestampEnd > highestTimestamp) {
					highestTimestamp = inputDef.TimestampEnd;
				}
			}

			result.timeLowest  = lowestTimestamp;
			result.timeHighest = highestTimestamp;
		}
		else {
			result.timeLowest  = 0;
			result.timeHighest = INT64_MAX;
		}

		result.timeDifference = result.timeHighest - result.timeLowest;
		result.timeShift      = result.timeLowest;

		if (mNode) {
			mNode.create<dv::CfgType::LONG>(
				"seek", 0, {0, result.timeDifference}, dv::CfgFlags::NORMAL, "Current playback point.");
		}

		// Create empty ConfigTree and parse the infoNode into it.
		auto infoTree = dv::Config::Tree::newTree();

		auto importedNode = infoTree.getRootNode();
		importedNode.importSubTreeFromXMLString(header->infoNode.c_str(), false);

		// Map shared amongst files to count up for each type and
		// give the event streams good names if conflicts arise.
		std::map<std::string, int> nameClashTracker;

		for (const auto &childNode : importedNode.getChildren()) {
			// Check type first.
			auto typeIdentifier = childNode.getString("typeIdentifier");

			if (typeIdentifier == dv::Types::nullIdentifier) {
				// Unknown type, skip.
				continue;
			}

			// Get original stream name.
			auto streamName = childNode.getString("originalOutputName");

			// Check if this collides with anything already there.
			if (nameClashTracker.count(streamName) == 0) {
				// Not present, add it and keep this name.
				nameClashTracker[streamName] = 1;
			}
			else {
				// Name already present, add _X suffix.
				streamName += ("_" + std::to_string(nameClashTracker[streamName]++));
			}

			// Set stream-level return information.
			dv::InputStream st;
			st.name           = streamName;
			st.typeIdentifier = typeIdentifier;
			st.id             = std::stoi(childNode.getName());

			result.streams.push_back(st);

			if (mNode) {
				// All info gathered, create corresponding output.
				// We only have the module node available here, so we just create
				// the sub-nodes and register the outputs later on.
				auto outputNode = mNode.getRelativeNode("outputs/" + streamName + "/");

				childNode.copyTo(outputNode);

				childNode.getRelativeNode("info/").copyTo(outputNode.getRelativeNode("info/"));
			}
		}

		// Get rid of temporary tree.
		infoTree.deleteTree();

		return (result);
	}

	static std::ifstream openFile(const boost::filesystem::path &fPath) {
		if (!boost::filesystem::exists(fPath) || !boost::filesystem::is_regular_file(fPath)) {
			throw std::runtime_error("File doesn't exist or cannot be accessed.");
		}

		if (fPath.extension().string() != ("." AEDAT4_FILE_EXTENSION)) {
			throw std::runtime_error("Unknown file extension '" + fPath.extension().string() + "'.");
		}

		std::ifstream fStream;

		// Enable exceptions on failure to open file.
		fStream.exceptions(std::ifstream::badbit | std::ifstream::failbit | std::ifstream::eofbit);

		fStream.open(fPath.string(), std::ios::in | std::ios::binary);

		return (fStream);
	}

	InputDecoder(dvModuleData mData, dv::RuntimeConfig *cfg, dv::Logger *logger) :
		moduleData(mData),
		config(cfg),
		log(logger) {
	}

	static std::shared_ptr<struct LZ4F_dctx_s> lz4InitDecompressionContext() {
		// Create LZ4 decompression context.
		struct LZ4F_dctx_s *ctx = nullptr;
		auto ret                = LZ4F_createDecompressionContext(&ctx, LZ4F_VERSION);
		if (ret != 0) {
			throw std::bad_alloc();
		}

		return (
			std::shared_ptr<struct LZ4F_dctx_s>(ctx, [](struct LZ4F_dctx_s *c) { LZ4F_freeDecompressionContext(c); }));
	}

	static std::shared_ptr<struct ZSTD_DCtx_s> zstdInitDecompressionContext() {
		// Create Zstd decompression context.
		struct ZSTD_DCtx_s *ctx = ZSTD_createDCtx();
		if (ctx == nullptr) {
			throw std::bad_alloc();
		}

		return (std::shared_ptr<struct ZSTD_DCtx_s>(ctx, [](struct ZSTD_DCtx_s *c) { ZSTD_freeDCtx(c); }));
	}

	void setup(std::istream *inStream, const InputInformation *inInfo) {
		inputStream = inStream;
		inputInfo   = inInfo;

		// Build LZ4 decompression context.
		if (inputInfo->compression == CompressionType::LZ4 || inputInfo->compression == CompressionType::LZ4_HIGH) {
			compressionLZ4.context = lz4InitDecompressionContext();
		}

		// Build Zstd decompression context.
		if (inputInfo->compression == CompressionType::ZSTD || inputInfo->compression == CompressionType::ZSTD_HIGH) {
			compressionZstd.context = zstdInitDecompressionContext();
		}
	}

	static void decompressLZ4(const char *dataPtr, size_t dataSize, std::vector<char> &decompressBuffer,
		struct LZ4F_dctx_s *decompressContext) {
		size_t decompressedSize = 0;
		size_t retVal           = 1;

		while ((dataSize > 0) && (retVal != 0)) {
			decompressBuffer.resize(decompressedSize + lz4CompressionChunkSize);
			size_t dstSize = lz4CompressionChunkSize;

			size_t srcSize = dataSize;

			retVal = LZ4F_decompress(
				decompressContext, decompressBuffer.data() + decompressedSize, &dstSize, dataPtr, &srcSize, nullptr);

			if (LZ4F_isError(retVal)) {
				throw std::runtime_error(std::string("LZ4 decompression error: ") + LZ4F_getErrorName(retVal));
			}

			decompressedSize += dstSize;

			dataSize -= srcSize;
			dataPtr += srcSize;
		}

		decompressBuffer.resize(decompressedSize);
	}

	static void decompressZstd(const char *dataPtr, size_t dataSize, std::vector<char> &decompressBuffer,
		struct ZSTD_DCtx_s *decompressContext) {
		auto decompressedSize = ZSTD_getFrameContentSize(dataPtr, dataSize);
		if (decompressedSize == ZSTD_CONTENTSIZE_UNKNOWN) {
			throw std::runtime_error("Zstd decompression error: unknown content size");
		}
		if (decompressedSize == ZSTD_CONTENTSIZE_ERROR) {
			throw std::runtime_error("Zstd decompression error: content size error");
		}

		decompressBuffer.resize(decompressedSize);

		auto retVal
			= ZSTD_decompressDCtx(decompressContext, decompressBuffer.data(), decompressedSize, dataPtr, dataSize);

		if (ZSTD_isError(retVal)) {
			throw std::runtime_error(std::string("Zstd decompression error: ") + ZSTD_getErrorName(retVal));
		}
		else {
			decompressBuffer.resize(retVal);
		}
	}

	void decompressData(const char *dataPtr, size_t dataSize) {
		if (inputInfo->compression == CompressionType::LZ4 || inputInfo->compression == CompressionType::LZ4_HIGH) {
			try {
				decompressLZ4(dataPtr, dataSize, decompressBuffer, compressionLZ4.context.get());
			}
			catch (const std::runtime_error &ex) {
				// Decompression error, ignore this packet.
#if defined(LZ4_VERSION_NUMBER) && LZ4_VERSION_NUMBER >= 10800
				LZ4F_resetDecompressionContext(compressionLZ4.context.get());
#else
				compressionLZ4.context.reset();
				compressionLZ4.context = lz4InitDecompressionContext();
#endif
				log->error << ex.what() << std::endl;
				return;
			}
		}

		if (inputInfo->compression == CompressionType::ZSTD || inputInfo->compression == CompressionType::ZSTD_HIGH) {
			try {
				decompressZstd(dataPtr, dataSize, decompressBuffer, compressionZstd.context.get());
			}
			catch (const std::runtime_error &ex) {
				// Decompression error, ignore this packet.
#if defined(ZSTD_VERSION_NUMBER) && ZSTD_VERSION_NUMBER >= 10400
				ZSTD_DCtx_reset(compressionZstd.context.get(), ZSTD_reset_session_only);
#else
				compressionZstd.context.reset();
				compressionZstd.context = zstdInitDecompressionContext();
#endif
				log->error << ex.what() << std::endl;
				return;
			}
		}
	}

	void run() {
		std::chrono::time_point<std::chrono::steady_clock> prepareDataStart = std::chrono::steady_clock::now();

		if (config->getString("mode") == "timeInterval") {
			int64_t startTimestamp = config->getLong("seek") + inputInfo->timeShift;
			int64_t endTimestamp   = startTimestamp + config->getLong("timeInterval") - 1;

			// Now we know how much time we need to extract, from when to when.
			// First let's see if there is even something for that time interval for
			// each of the streams.
			for (const auto &st : inputInfo->streams) {
				std::vector<FileDataDefinitionT> packetsToRead;

				for (const auto &def : inputInfo->dataTable->Table) {
					if (def.PacketInfo.StreamID() != st.id) {
						// Not this stream.
						continue;
					}

					if (def.TimestampStart <= endTimestamp && def.TimestampEnd >= startTimestamp) {
						// Packet time range overlaps with searched for range.
						packetsToRead.push_back(def);
					}
				}

				if (packetsToRead.empty()) {
					// No data for this stream and time interval, skip.
					continue;
				}

				for (const auto &pkt : packetsToRead) {
					inputStream->seekg(pkt.ByteOffset, std::ios_base::beg);

					readBuffer.resize(static_cast<size_t>(pkt.PacketInfo.Size()));

					inputStream->read(readBuffer.data(), pkt.PacketInfo.Size());

					size_t dataSize     = readBuffer.size();
					const char *dataPtr = readBuffer.data();

					// Decompress packet.
					if (inputInfo->compression != dv::CompressionType::NONE) {
						decompressData(dataPtr, dataSize);

						dataSize = decompressBuffer.size();
						dataPtr  = decompressBuffer.data();
					}

					// dataPtr and dataSize now contain the uncompressed, raw flatbuffer.
					if (!flatbuffers::BufferHasIdentifier(dataPtr, st.typeIdentifier.c_str(), true)) {
						// Wrong type identifier for this flatbuffer (file_identifier field).
						// This should never happen, ignore packet.
						log->error.format("Wrong type identifier for packet: '%s', expected: '%s'.",
							flatbuffers::GetBufferIdentifier(dataPtr, true), st.typeIdentifier);
						return;
					}

					auto fbPtr = flatbuffers::GetSizePrefixedRoot<void>(dataPtr);

					auto outPacket = dvModuleOutputAllocate(moduleData, st.name.c_str());

					if (st.type.unpackTimeElementRange(outPacket->obj, fbPtr, {startTimestamp, endTimestamp, -1})) {
						// Early commit in some cases, like single elements.
						dvModuleOutputCommit(moduleData, st.name.c_str());
					}
				}

				// Full packet commit.
				dvModuleOutputCommit(moduleData, st.name.c_str());
			}

			int64_t nextTimestamp = endTimestamp + 1;

			// If the next read would be outside existing time in this file, we've reached the end.
			if (nextTimestamp > inputInfo->timeHighest) {
				throw std::ios::failure("EOF");
			}

			config->setLong("seek", nextTimestamp - inputInfo->timeShift);
		}
		else {
			// TODO: implement this later.
			int64_t requiredElements = config->getLong("eventNumber");
		}

		std::chrono::time_point<std::chrono::steady_clock> prepareDataEnd = std::chrono::steady_clock::now();
		const auto timeSpentPreparingData
			= std::chrono::duration_cast<std::chrono::microseconds>(prepareDataEnd - prepareDataStart).count();

		if (timeSpentPreparingData >= config->getLong("timeDelay")) {
			log->warning("Cannot keep up with requested time delay during playback.");
		}
		else {
			// Configurable time delay for real-time playback.
			std::this_thread::sleep_for(
				std::chrono::microseconds(config->getLong("timeDelay") - timeSpentPreparingData));
		}
	}
};

} // namespace dv

#endif // DV_INPUT_HPP
