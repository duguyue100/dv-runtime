#include "dv_input.hpp"

#include <fstream>
#include <thread>

class InFile : public dv::ModuleBase {
private:
	static void regenerateOutputs(const std::string &path, dvModuleData mData, dv::Config::Node mNode) {
		try {
			auto fStream = dv::InputDecoder::openFile(path);

			// Cleanup all added outputs.
			dvModuleRegisterOutput(mData, "REMOVE", "ALL");

			// Re-generate outputs list from currently open file.
			dv::InputDecoder::parseHeader(fStream, mNode);
		}
		catch (const std::exception &ex) {
			libcaer::log::log(
				libcaer::log::logLevel::ERROR, mNode.getName().c_str(), "File '%s': %s", path.c_str(), ex.what());
		}
	}

	static void fileChangeListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		auto mData = static_cast<dvModuleData>(userData);
		auto mNode = dv::Cfg::Node(node);

		if (event == DVCFG_ATTRIBUTE_MODIFIED && changeType == DVCFG_TYPE_STRING && caerStrEquals(changeKey, "file")) {
			if (mNode.getBool("isRunning") || mNode.getBool("running")) {
				// Stop module. Change to 'isRunning' will continue file parsing.
				mNode.putBool("running", false);
			}
			else {
				// Module not running, let's update the outputs.
				regenerateOutputs(changeValue.string, mData, mNode);
			}

			// On file change, always reset seek.
			mNode.putLong("seek", 0); // Ensure reset to zero on reload.
		}
	}

	static void isRunningChangeListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
		auto mData = static_cast<dvModuleData>(userData);
		auto mNode = dv::Cfg::Node(node);

		if (event == DVCFG_ATTRIBUTE_MODIFIED && changeType == DVCFG_TYPE_BOOL && caerStrEquals(changeKey, "isRunning")
			&& !changeValue.boolean) {
			regenerateOutputs(mNode.getString("file"), mData, node);
		}
	}

	std::ifstream fileStream;
	dv::InputInformation fileInfo;
	dv::InputDecoder fileDecoder;
	bool hitEOF;

public:
	static const char *getDescription() {
		return ("Read AEDAT 4.0 data from a file.");
	}

	static void getConfigOptions(dv::RuntimeConfig &config) {
		config.add("file", dv::ConfigOption::fileOpenOption("File to load data from.", AEDAT4_FILE_EXTENSION));
		config.add("mode", dv::ConfigOption::listOption("Time or event-based readout.", 0, {"timeInterval"}));
		// TODO: re-add "eventNumber" mode later.

		config.add("timeInterval",
			dv::ConfigOption::longOption("Time interval to extract for each packet of data (in µs).", 10000));
		// config.add("eventNumber",
		// dv::ConfigOption::longOption("Number of elements to extract for each packet of data.", 2000));

		config.add("timeDelay", dv::ConfigOption::longOption("Time between consecutive packets (in µs).", 10000));

		config.add("pause", dv::ConfigOption::boolOption("Pause playback.", false));
		config.add("loop", dv::ConfigOption::boolOption("Loop playback.", false));
		config.add("seek", dv::ConfigOption::longOption("Current playback point.", 0, 0, INT64_MAX));

		config.setPriorityOptions({"file", "loop"});
	}

	static void advancedStaticInit(dvModuleData mData) {
		auto mNode = dv::Cfg::Node(mData->moduleNode);

		// Add read-only fileSize.
		mNode.create<dv::CfgType::LONG>(
			"fileSize", 0, {0, INT64_MAX}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "File size in bytes.");

		// On changes to 'files' attribute, re-generate the list of inputs,
		// or shut down the module, to allow user to start with new selection.
		mNode.addAttributeListener(mData, &fileChangeListener);

		// On changes to 'isRunning = false', regenerate the outputs.
		mNode.addAttributeListener(mData, &isRunningChangeListener);

		// Ensure outputs are generated at startup.
		regenerateOutputs(mNode.getString("file"), mData, mNode);
	}

	InFile() : fileDecoder(moduleData, &config, &log), hitEOF(false) {
		try {
			fileStream = dv::InputDecoder::openFile(config.getString("file"));

			// Cleanup all added outputs.
			dvModuleRegisterOutput(moduleData, "REMOVE", "ALL");

			// Re-generate outputs list from currently open file.
			fileInfo = dv::InputDecoder::parseHeader(fileStream, moduleNode);
		}
		catch (const std::exception &ex) {
			auto exMsg = boost::format("File '%s': %s") % config.getString("file") % ex.what();
			throw std::runtime_error(exMsg.str());
		}

		// Check if there are any viable streams in this file.
		if (fileInfo.streams.empty()) {
			auto exMsg = boost::format("File '%s': no valid streams found.") % config.getString("file");
			throw std::runtime_error(exMsg.str());
		}

		// Register outputs with module IO system, resolve type references.
		for (auto &st : fileInfo.streams) {
			dvModuleRegisterOutput(moduleData, st.name.c_str(), st.typeIdentifier.c_str());

			st.type = dvTypeSystemGetInfoByIdentifier(st.typeIdentifier.c_str());
		}

		// Setup decoder once all info is there.
		fileDecoder.setup(&fileStream, &fileInfo);

		// Log details.
		log.debug.format("Compression: %s, data table position: %d.", dv::EnumNameCompressionType(fileInfo.compression),
			fileInfo.dataTablePosition);

		if (fileInfo.dataTable->Table.empty()) {
			auto exMsg = boost::format("File '%s': no data table found, possibly corrupted file. Please use the File "
									   "converter to analyze and fix this.")
						 % config.getString("file");
			throw std::runtime_error(exMsg.str());
		}

		// Done.
		log.info << "Opened file '" << config.getString("file") << "' successfully." << std::endl;
	}

	~InFile() override {
		// Looping support: at the end of file, we shut down, always,
		// to simplify the module. And so to loop we simply automatically
		// start running again.
		if (hitEOF && config.getBool("loop")) {
			config.setBool("running", true);

			log.info << "Loop mode set, starting playback again." << std::endl;
		}
	}

	void run() override {
		if (config.getBool("pause")) {
			// Do nothing, 50ms delay.
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
			return;
		}

		try {
			fileDecoder.run();
		}
		catch (const std::ios::failure &ex) {
			// Unrecoverable IO problem.
			config.setBool("running", false);

			if ((fileStream.eof()) || (strncmp(ex.what(), "EOF", 3) == 0)) {
				// EOF is normal and just shuts the module down.
				log.info << "End of File reached, shutting down." << std::endl;

				// Go back to beginning of file on next run.
				config.setLong("seek", 0);

				// Remember we hit end-of-file for shutdown operation.
				hitEOF = true;
			}
			else {
				// Actual failure, throw up.
				throw;
			}
		}
	}
};

registerModuleClass(InFile)
