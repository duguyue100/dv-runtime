#ifndef DV_RUNTIME_CONVERTER_H
#define DV_RUNTIME_CONVERTER_H

#include "aedat2Parser.hpp"
#include "aedat31Parser.hpp"
#include "aedat3Parser.hpp"
#include "dv-sdk/module.hpp"

#include <boost/filesystem.hpp>
#include <fstream>

class Converter : public dv::ModuleBase {
private:
    static constexpr int VERSION_LINE_MAX_LENGTH = 15;

    std::unique_ptr<Aedat2Parser> aedat2Parser;
    std::unique_ptr<Aedat31Parser> aedat31Parser;
    std::unique_ptr<Aedat3Parser> aedat3Parser;

    std::ifstream inputFile;
    int64_t fileSize      = 0;
    int64_t processedSize = 0;
    int aedatVersion;

    int readAedatVersion();

public:
    static const char *getDescription() {
        return "Reads aedat 2 / 3 files and outputs them, so that they can be re-written to a newer format.";
    }

    static void addOutputs(dv::OutputDefinitionList &out) {
        out.addEventOutput("events");
        out.addFrameOutput("frames");
        out.addIMUOutput("imu");
        out.addTriggerOutput("triggers");
    }

    static void getConfigOptions(dv::RuntimeConfig &config) {
        config.add("file", dv::ConfigOption::fileOpenOption("Aedat file to convert", "aedat,dat,*"));
        config.add("fileSize", dv::ConfigOption::statisticOption("Size of the file the module is currently parsing"));
        config.add(
            "processedSize", dv::ConfigOption::statisticOption("Amount of data already processed in current file"));
        config.setPriorityOptions({"running", "file", "fileSize", "processedSize"});
    }

    Converter();

    void run() override;

    void aedat2Init(const std::string &filePath);
    void aedat31Init(const std::string &filePath);
    void aedat3Init(const std::string &filePath);

    void aedat2Run();
    void aedat3Run();
    void aedat31Run();
};

registerModuleClass(Converter)

#endif // DV_RUNTIME_CONVERTER_H
