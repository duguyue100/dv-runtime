//
// Created by Lennart Walger on 2019-08-26.
//
#ifndef AEDAT3PARSER_HPP
#define AEDAT3PARSER_HPP

#include "aedat3types.hpp"
#include "dv-sdk/log.hpp"
#include "dv-sdk/module_io.hpp"
#include "dv-sdk/processing.hpp"

#include <fstream>

class Aedat3Parser {
private:
    std::ifstream file;
    dv::Logger *log;

    int64_t startingTS = 0;
    /**
     * @brief parse an Aedat3.0 file header
     */
    int parse_aedat3_header();
    /**
     * @brief parses an Aedat3.1 specialEventPacket and commits it to the triggerOut stream
     * @param header
     * @param triggerOut
     */
    int parse_specialEvent_packet(
        char *header, dv::OutputVectorDataWrapper<dv::TriggerPacket, dv::TriggerT> &triggerOut);
    /**
     * @brief parses an Aedat3.1 polarityEventPacket and commits all valid events to the eventOut stream
     * @param header
     * @param eventOut
     */
    int parse_polarityEvent_packet(char *header, dv::OutputVectorDataWrapper<dv::EventPacket, dv::Event> &eventOut);
    /**
     * @brief parses an Aedat3.1 frameEventPacket and commits the frame to the frameOut stream
     * @param header
     * @param frameOut
     */
    int parse_frameEvent_packet(char *header, dv::OutputDataWrapper<dv::Frame> &frameOut);
    /**
     * @brief parses an Aedat3.1 imu6EventPacket and commits it to the imuOut stream
     * @param header
     * @param imuOut
     */
    int parse_imu6Event_packet(char *header, dv::OutputVectorDataWrapper<dv::IMUPacket, dv::IMUT> &imuOut);

public:
    short formatID;
    short sourceID;

    size_t eventSizeX;
    size_t eventSizeY;
    size_t apsSizeX;
    size_t apsSizeY;

    Aedat3Parser(const std::string &filePath, dv::Logger *pLog) : log(pLog) {
        file = std::ifstream(filePath, std::ios::binary);
        parse_aedat3_header();
    }

    int get_data(dv::OutputVectorDataWrapper<dv::EventPacket, dv::Event> &eventOut,
        dv::OutputDataWrapper<dv::Frame> &frameOut, dv::OutputVectorDataWrapper<dv::IMUPacket, dv::IMUT> &imuOut,
        dv::OutputVectorDataWrapper<dv::TriggerPacket, dv::TriggerT> &triggerOut);
};

#endif // AEDAT3PARSER_HPP
