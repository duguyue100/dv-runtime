#include "dv-sdk/module.hpp"
#include "dv-sdk/processing.hpp"

#include <opencv2/imgproc.hpp>

class FlipRotate : public dv::ModuleBase {
private:
	bool eventsFlipVertically   = false;
	bool eventsFlipHorizontally = false;
	bool framesFlipVertically   = false;
	bool framesFlipHorizontally = false;
	int degreeOfRotation        = 0;
	float cosOfDegree           = 0.0;
	float sinOfDegree           = 0.0;

public:
	FlipRotate() {
		if (inputs.getEventInput("events").isConnected()) {
			outputs.getEventOutput("events").setup(inputs.getEventInput("events"));
		}
		else {
			outputs.getEventOutput("events").setup(0, 0, "input not connected");
		}

		if (inputs.getFrameInput("frames").isConnected()) {
			outputs.getFrameOutput("frames").setup(inputs.getFrameInput("frames"));
		}
		else {
			outputs.getFrameOutput("frames").setup(0, 0, "input not connected");
		}
	}

	static const char *getDescription() {
		return "Module to flip and rotate Events and Frames horizonally, vertically or setting a degree of rotation";
	}

	static void addInputs(dv::InputDefinitionList &in) {
		in.addEventInput("events", true);
		in.addFrameInput("frames", true);
	}

	static void addOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
		out.addFrameOutput("frames");
	}

	static void getConfigOptions(dv::RuntimeConfig &config) {
		config.add("eventFlipVertically", dv::ConfigOption::boolOption("Flip events vertically"));
		config.add("eventFlipHorizontally", dv::ConfigOption::boolOption("Flip events horizontally"));
		config.add("frameFlipVertically", dv::ConfigOption::boolOption("Flip frames vertically"));
		config.add("frameFlipHorizontally", dv::ConfigOption::boolOption("Flip frames horizontally"));
		config.add(
			"degreeOfRotation", dv::ConfigOption::intOption("Degree of rotation from -180° to 180°", 0, -180, 180));
		config.setPriorityOptions({"degreeOfRotation", "eventFlipVertically", "eventFlipHorizontally",
			"frameFlipVertically", "frameFlipHorizontally"});
	}

	void run() override {
		//------------------------------------------------------------------------
		// Events management
		auto inEvent = inputs.getEventInput("events");

		if (inEvent.isConnected() && inEvent.events()) {
			auto outEvent = outputs.getEventOutput("events");
			int height    = inputs.getEventInput("events").sizeY();
			int width     = inputs.getEventInput("events").sizeX();
			int centerX   = (width - 1) / 2;
			int centerY   = (height - 1) / 2;

			for (const auto &event : inEvent.events()) {
				int16_t x;
				int16_t y;

				// flip of events
				if (eventsFlipVertically) {
					y = static_cast<int16_t>(height - 1 - event.y());
				}
				else {
					y = event.y();
				}

				if (eventsFlipHorizontally) {
					x = static_cast<int16_t>(width - 1 - event.x());
				}
				else {
					x = event.x();
				}

				// rotation of events
				if (double(degreeOfRotation) != 0) {
					double dx = static_cast<double>(x - centerX);
					double dy = static_cast<double>(y - centerY);

					x = static_cast<int16_t>(
						dx * static_cast<double>(cosOfDegree) - dy * static_cast<double>(sinOfDegree) + centerX);
					y = static_cast<int16_t>(
						dx * static_cast<double>(sinOfDegree) + dy * static_cast<double>(cosOfDegree) + centerY);

					if (x >= width || x < 0) {
						continue;
					}
					if (y >= height || y < 0) {
						continue;
					}
				}

				outEvent << dv::Event(event.timestamp(), x, y, event.polarity());
			}

			outEvent << dv::commit;
		}

		//------------------------------------------------------------------------
		// Frames management
		auto inFrame = inputs.getFrameInput("frames");
		auto frame   = inFrame.frame();

		if (inFrame.isConnected() && frame) {
			auto outFrame = outputs.getFrameOutput("frames").frame();
			int height    = inputs.getFrameInput("frames").sizeY();
			int width     = inputs.getFrameInput("frames").sizeX();
			int centerX   = (width - 1) / 2;
			int centerY   = (height - 1) / 2;

			// Setup output frame. Same size.
			outFrame.setROI(frame.roi());
			outFrame.setFormat(frame.format());
			outFrame.setTimestamp(frame.timestamp());
			outFrame.setTimestampStartOfExposure(frame.timestampStartOfExposure());
			outFrame.setTimestampEndOfExposure(frame.timestampEndOfExposure());
			outFrame.setTimestampStartOfFrame(frame.timestampStartOfFrame());
			outFrame.setTimestampEndOfFrame(frame.timestampEndOfFrame());

			cv::Mat frameFlip;
			cv::Mat frameRotate;

			// flip of frame
			if (framesFlipVertically && framesFlipHorizontally) {
				cv::flip(*frame.getMatPointer(), frameFlip, -1);
			}
			else if (framesFlipVertically && not framesFlipHorizontally) {
				cv::flip(*frame.getMatPointer(), frameFlip, 0);
			}
			else if (not framesFlipVertically && framesFlipHorizontally) {
				cv::flip(*frame.getMatPointer(), frameFlip, 1);
			}
			else {
				frameFlip = *frame.getMatPointer();
			}

			// rotation of the frame
			cv::Size2d sizeImage = cv::Size_(width, height);
			cv::Point2d center   = cv::Point_(centerX, centerY);
			if (static_cast<double>(degreeOfRotation) != 0) {
				auto M = cv::getRotationMatrix2D(center, static_cast<double>(-degreeOfRotation), 1);
				cv::warpAffine(frameFlip, frameRotate, M, sizeImage);
			}
			else {
				frameRotate = frameFlip;
			}

			outFrame.commitMat(frameRotate);
		}
	}

	void configUpdate() override {
		eventsFlipVertically   = config.getBool("eventFlipVertically");
		eventsFlipHorizontally = config.getBool("eventFlipHorizontally");
		framesFlipVertically   = config.getBool("frameFlipVertically");
		framesFlipHorizontally = config.getBool("frameFlipHorizontally");
		degreeOfRotation       = config.getInt("degreeOfRotation");

		cosOfDegree = std::cos(degreeOfRotation * M_PI / 180);
		sinOfDegree = std::sin(degreeOfRotation * M_PI / 180);
	}
};

registerModuleClass(FlipRotate)
