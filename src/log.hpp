#ifndef LOG_HPP_
#define LOG_HPP_

#include "dv-sdk/utils.h"

#include <array>
#include <atomic>
#include <boost/algorithm/string/join.hpp>
#include <string>

#define DV_LOG_FILE_NAME ".dv-logger.txt"

namespace dv {

static std::array<const std::string, 8> logLevelNames
	= {"EMERGENCY", "ALERT", "CRITICAL", "ERROR", "WARNING", "NOTICE", "INFO", "DEBUG"};

static inline std::string logLevelNamesCommaList() {
	return (boost::algorithm::join(logLevelNames, ","));
}

static inline int logLevelNameToInteger(const std::string &ls) {
	if (ls == logLevelNames[CAER_LOG_EMERGENCY]) {
		return (CAER_LOG_EMERGENCY);
	}
	else if (ls == logLevelNames[CAER_LOG_ALERT]) {
		return (CAER_LOG_ALERT);
	}
	else if (ls == logLevelNames[CAER_LOG_CRITICAL]) {
		return (CAER_LOG_CRITICAL);
	}
	else if (ls == logLevelNames[CAER_LOG_ERROR]) {
		return (CAER_LOG_ERROR);
	}
	else if (ls == logLevelNames[CAER_LOG_WARNING]) {
		return (CAER_LOG_WARNING);
	}
	else if (ls == logLevelNames[CAER_LOG_NOTICE]) {
		return (CAER_LOG_NOTICE);
	}
	else if (ls == logLevelNames[CAER_LOG_INFO]) {
		return (CAER_LOG_INFO);
	}
	else {
		return (CAER_LOG_DEBUG);
	}
}

static inline std::string logLevelIntegerToName(const int li) {
	return (logLevelNames[li]);
}

struct LogBlock {
	std::string logPrefix;
	std::atomic_int32_t logLevel;
};

void LoggerInit(void);

// Part of SDK due to dvLog() having to be part of it.
LIB_PUBLIC_VISIBILITY void LoggerSet(const LogBlock *logger);
LIB_PUBLIC_VISIBILITY const LogBlock *LoggerGet();

} // namespace dv

#endif /* LOG_HPP_ */
