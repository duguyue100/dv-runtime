#include "main.hpp"

#include "dv-sdk/cross/portable_io.h"

#include "config.hpp"
#include "config_server/config_server.h"
#include "devices_discovery.hpp"
#include "log.hpp"
#include "module.hpp"
#include "modules_discovery.hpp"
#include "service.hpp"

#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <chrono>
#include <csignal>
#include <iostream>
#include <mutex>

// If Boost version recent enough, enable better stack traces on segfault.
#include <boost/version.hpp>

#if defined(BOOST_VERSION) && (BOOST_VERSION / 100000) == 1 && (BOOST_VERSION / 100 % 1000) >= 66
#	define BOOST_HAS_STACKTRACE 1
#else
#	define BOOST_HAS_STACKTRACE 0
#endif

#if BOOST_HAS_STACKTRACE
#	define BOOST_STACKTRACE_GNU_SOURCE_NOT_REQUIRED 1
#	include <boost/stacktrace.hpp>
#elif defined(OS_LINUX)
#	include <execinfo.h>
#endif

static void mainRunner();
static void mainSegfaultHandler(int signum);
static void mainShutdownHandler(int signum);
static void systemRunningListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
	const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue);

void dv::addModule(const std::string &name, const std::string &library, bool startModule) {
	std::scoped_lock lock(dv::MainData::getGlobal().modulesLock);

	auto restoreLogger = dv::LoggerGet();

	try {
		dv::MainData::getGlobal().modules.try_emplace(name, std::make_shared<dv::Module>(name, library));

		if (startModule) {
			dv::MainData::getGlobal().modules.at(name)->startThread();
		}
	}
	catch (const std::exception &ex) {
		dv::Log(dv::logLevel::CRITICAL, "addModule(): '%s', removing module.", ex.what());
		dv::Cfg::GLOBAL.getNode("/mainloop/" + name + "/").removeNode();
	}

	dv::LoggerSet(restoreLogger);
}

void dv::removeModule(const std::string &name) {
	// Check that module has stopped.
	auto moduleNode = dv::Cfg::GLOBAL.getNode("/mainloop/" + name + "/");

shutdownModule:
	// First we stop it ourselves to be sure and wait for isRunning=false.
	moduleNode.put<dv::CfgType::BOOL>("running", false);

	while (moduleNode.get<dv::CfgType::BOOL>("isRunning")) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	// Now we check again but do so while holding the modulesLock.
	// It could happen that the module we're trying to remove is
	// also trying to start up right now, due to a previous running=true;
	// it will either try after we do take the lock, meaning it
	// will fail to take it itself and notice it should exit and do so,
	// or it will take the lock and start up: if it succeeds, isRunning
	// will be true, so we check for that after we finally take the
	// lock successfully, and if it is, we do the shutdown again.
	{
		std::scoped_lock lock(dv::MainData::getGlobal().modulesLock);

		if (moduleNode.get<dv::CfgType::BOOL>("isRunning")) {
			goto shutdownModule;
		}

		auto restoreLogger = dv::LoggerGet();

		dv::MainData::getGlobal().modules.at(name)->stopThread();

		dv::MainData::getGlobal().modules.erase(name);

		dv::LoggerSet(restoreLogger);
	}
}

static void mainRunner() {
	// Setup internal function pointers for public support library.
	auto libFuncPtrs = &dv::MainData::getGlobal().libFunctionPointers;

	libFuncPtrs->getTypeInfoCharString = [typeSystem = &dv::MainData::getGlobal().typeSystem](const char *cs,
											 const dv::Module *m) { return (typeSystem->getTypeInfo(cs, m)); };
	libFuncPtrs->getTypeInfoIntegerID  = [typeSystem = &dv::MainData::getGlobal().typeSystem](uint32_t ii,
                                            const dv::Module *m) { return (typeSystem->getTypeInfo(ii, m)); };

	libFuncPtrs->registerType      = &dv::Module::registerType;
	libFuncPtrs->registerOutput    = &dv::Module::registerOutput;
	libFuncPtrs->registerInput     = &dv::Module::registerInput;
	libFuncPtrs->outputAllocate    = &dv::Module::outputAllocate;
	libFuncPtrs->outputCommit      = &dv::Module::outputCommit;
	libFuncPtrs->inputGet          = &dv::Module::inputGet;
	libFuncPtrs->inputAdvance      = &dv::Module::inputAdvance;
	libFuncPtrs->inputDismiss      = &dv::Module::inputDismiss;
	libFuncPtrs->outputGetInfoNode = &dv::Module::outputGetInfoNode;
	libFuncPtrs->inputGetInfoNode  = &dv::Module::inputGetInfoNode;
	libFuncPtrs->inputIsConnected  = &dv::Module::inputIsConnected;

	dv::SDKLibInit(libFuncPtrs);

// Install signal handler for global shutdown.
#if defined(OS_WINDOWS)
	if (signal(SIGTERM, &mainShutdownHandler) == SIG_ERR) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGTERM. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	if (signal(SIGINT, &mainShutdownHandler) == SIG_ERR) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGINT. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	if (signal(SIGBREAK, &mainShutdownHandler) == SIG_ERR) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGBREAK. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	if (signal(SIGSEGV, &mainShutdownHandler) == SIG_ERR) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGSEGV. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	if (signal(SIGABRT, &mainShutdownHandler) == SIG_ERR) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGABRT. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	// Disable closing of the console window where DV is executing.
	// While we do catch the signal (SIGBREAK) that such an action generates, it seems
	// we can't reliably shut down within the hard time window that Windows enforces when
	// pressing the close button (X in top right corner usually). This seems to be just
	// 5 seconds, and we can't guarantee full shutdown (USB, file writing, etc.) in all
	// cases within that time period (multiple cameras, modules etc. make this worse).
	// So we just disable that and force the user to CTRL+C, which works fine.
	HWND consoleWindow = GetConsoleWindow();
	if (consoleWindow != nullptr) {
		HMENU systemMenu = GetSystemMenu(consoleWindow, false);
		EnableMenuItem(systemMenu, SC_CLOSE, MF_GRAYED);
	}
#else
	struct sigaction shutdown;

	shutdown.sa_handler = &mainShutdownHandler;
	shutdown.sa_flags   = 0;
	sigemptyset(&shutdown.sa_mask);
	sigaddset(&shutdown.sa_mask, SIGTERM);
	sigaddset(&shutdown.sa_mask, SIGINT);

	if (sigaction(SIGTERM, &shutdown, nullptr) == -1) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGTERM. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	if (sigaction(SIGINT, &shutdown, nullptr) == -1) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGINT. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	struct sigaction segfault;

	segfault.sa_handler = &mainSegfaultHandler;
	segfault.sa_flags   = 0;
	sigemptyset(&segfault.sa_mask);
	sigaddset(&segfault.sa_mask, SIGSEGV);
	sigaddset(&segfault.sa_mask, SIGABRT);

	if (sigaction(SIGSEGV, &segfault, nullptr) == -1) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGSEGV. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	if (sigaction(SIGABRT, &segfault, nullptr) == -1) {
		dv::Log(dv::logLevel::EMERGENCY, "Failed to set signal handler for SIGABRT. Error: %d.", errno);
		exit(EXIT_FAILURE);
	}

	// Ignore SIGPIPE.
	signal(SIGPIPE, SIG_IGN);
#endif

	// Ensure core nodes exist.
	auto systemNode   = dv::Cfg::GLOBAL.getNode("/system/");
	auto mainloopNode = dv::Cfg::GLOBAL.getNode("/mainloop/");

	// Add core attributes.
	systemNode.create<dv::CfgType::STRING>(
		"version", DV_PROJECT_VERSION, {0, 32}, dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Runtime version.");

	mainloopNode.attributeModifierGUISupport();

	// Support device discovery.
	auto devicesNode = systemNode.getRelativeNode("devices/");

	devicesNode.create<dv::CfgType::BOOL>("updateAvailableDevices", false, {},
		dv::CfgFlags::NORMAL | dv::CfgFlags::NO_EXPORT, "Update available devices list.");
	devicesNode.attributeModifierButton("updateAvailableDevices", "Update");
	devicesNode.addAttributeListener(nullptr, &dv::DevicesUpdateListener);

	dv::DevicesUpdateList(); // Run once at startup.

	// Initialize module related configuration.
	auto modulesNode = systemNode.getRelativeNode("modules/");

	// Additional modules search directories.
	modulesNode.create<dv::CfgType::STRING>("modulesSearchPath", "", {0, 64 * PATH_MAX}, dv::CfgFlags::NORMAL,
		"Additional directories to search loadable modules in, separated by '|'.");

	modulesNode.create<dv::CfgType::BOOL>("updateModulesInformation", false, {},
		dv::CfgFlags::NORMAL | dv::CfgFlags::NO_EXPORT, "Update modules information.");
	modulesNode.attributeModifierButton("updateModulesInformation", "Update");
	modulesNode.addAttributeListener(nullptr, &dv::ModulesUpdateInformationListener);

	dv::ModulesUpdateInformation(); // Run once at startup.

	// Allow user-driven configuration write-back.
	systemNode.create<dv::CfgType::BOOL>("writeConfiguration", false, {},
		dv::CfgFlags::NORMAL | dv::CfgFlags::NO_EXPORT, "Write current configuration to XML config file.");
	systemNode.attributeModifierButton("writeConfiguration", "Write to file");
	systemNode.addAttributeListener(nullptr, &dv::ConfigWriteBackListener);

	// Allow system running status control (shutdown).
	systemNode.create<dv::CfgType::BOOL>(
		"running", true, {}, dv::CfgFlags::NORMAL | dv::CfgFlags::NO_EXPORT, "Global system start/stop.");
	systemNode.addAttributeListener(nullptr, &systemRunningListener);

	// Start the configuration server thread for run-time config changes.
	ConfigServer cfgServer{};

	try {
		// Open listening socket, configure TLS.
		cfgServer.serviceConfigure();
	}
	catch (const boost::system::system_error &) {
		return;
	}

	// Add each module defined in configuration to runnable modules.
	// Do not start them yet.
	for (const auto &child : mainloopNode.getChildren()) {
		dv::addModule(child.getName(), child.get<dv::CfgType::STRING>("moduleLibrary"), false);
	}

	// Start modules after having added them all, so that connections between
	// each other may correctly resolve right away.
	{
		std::scoped_lock lock(dv::MainData::getGlobal().modulesLock);

		for (const auto &m : dv::MainData::getGlobal().modules) {
			m.second->startThread();
		}
	}

	// Enable config server.
	cfgServer.threadStart();

	// Main thread now works as updater (sleeps most of the time).
	size_t systemSleepCounter = 0;

	while (dv::MainData::getGlobal().systemRunning) {
		if (systemSleepCounter == 20) {
			systemSleepCounter = 0;

			dv::Cfg::GLOBAL.attributeUpdaterRun();
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		systemSleepCounter++;
	}

	dv::Cfg::GLOBAL.attributeUpdaterRemoveAll();

	// After shutting down the updater, also shutdown the config server thread,
	// to ensure no more changes can happen.
	cfgServer.threadStop();

	// Write config back on shutdown, after config server is disabled (no more
	// changes), but before we set running to false on all modules and force
	// their shutdown (so that correct run status is written to file).
	dv::ConfigWriteBack();

	// Separate input modules from others.
	std::vector<dv::Cfg::Node> inputModules;
	std::vector<dv::Cfg::Node> normalModules;

	for (auto &child : mainloopNode.getChildren()) {
		if (child.existsRelativeNode("inputs/") && !child.getRelativeNode("inputs/").getChildren().empty()) {
			normalModules.push_back(child);
		}
		else {
			inputModules.push_back(child);
		}
	}

	// Terminate all normal modules.
	for (auto &child : normalModules) {
		child.put<dv::CfgType::BOOL>("running", false);
	}

	// Remove all normal modules.
	for (auto &child : normalModules) {
		dv::removeModule(child.getName());
	}

	// Terminate all input modules.
	for (auto &child : inputModules) {
		child.put<dv::CfgType::BOOL>("running", false);
	}

	// Remove all input modules.
	for (auto &child : inputModules) {
		dv::removeModule(child.getName());
	}

	// Remove attribute listeners for clean shutdown.
	systemNode.removeAttributeListener(nullptr, &systemRunningListener);
	systemNode.removeAttributeListener(nullptr, &dv::ConfigWriteBackListener);
	modulesNode.removeAttributeListener(nullptr, &dv::ModulesUpdateInformationListener);
	devicesNode.removeAttributeListener(nullptr, &dv::DevicesUpdateListener);
}

static void mainSegfaultHandler(int signum) {
	signal(signum, SIG_DFL);

// Segfault or abnormal termination, try to print a stack trace if possible.
#if BOOST_HAS_STACKTRACE
	std::cerr << boost::stacktrace::stacktrace();
#elif defined(OS_LINUX)
	void *traces[128];
	int tracesActualNum = backtrace(traces, 128);
	backtrace_symbols_fd(traces, tracesActualNum, STDERR_FILENO);
#endif

	raise(signum);
}

static void mainShutdownHandler(int signum) {
	UNUSED_ARGUMENT(signum);

	// Ensure a reasonably recent version of the config is always written out.
	dv::ConfigWriteBack();

	// Set the system running flag to false on SIGTERM and SIGINT (CTRL+C) for global shutdown.
	dv::MainData::getGlobal().systemRunning = false;
}

static void systemRunningListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
	const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
	UNUSED_ARGUMENT(node);
	UNUSED_ARGUMENT(userData);
	UNUSED_ARGUMENT(changeValue);

	if (event == DVCFG_ATTRIBUTE_MODIFIED && changeType == DVCFG_TYPE_BOOL && caerStrEquals(changeKey, "running")) {
		// Ensure a reasonably recent version of the config is always written out.
		dv::ConfigWriteBack();

		// Set the system running flag to false for global shutdown.
		dv::MainData::getGlobal().systemRunning = false;
	}
}

int main(int argc, char **argv) {
	// Initialize config storage from file, support command-line overrides.
	dv::ConfigInit(argc, argv);

	// Initialize logging sub-system.
	dv::LoggerInit();

	// Start the DV runtime. Can be as a background service or console application.
	dv::ServiceInit(&mainRunner);

	return (EXIT_SUCCESS);
}
