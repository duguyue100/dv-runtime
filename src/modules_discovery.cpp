#include "modules_discovery.hpp"

#include "dv-sdk/cross/portable_io.h"

#include "log.hpp"

#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/tokenizer.hpp>
#include <mutex>
#include <regex>
#include <unordered_set>
#include <vector>

#define INTERNAL_XSTR(a) INTERNAL_STR(a)
#define INTERNAL_STR(a) #a

static std::pair<dv::ModuleLibrary, dvModuleInfo> InternalLoadLibrary(boost::filesystem::path modulePath);

static struct {
	std::vector<boost::filesystem::path> modulePaths;
	std::mutex modulePathsMutex;
} glModuleData;

std::pair<dv::ModuleLibrary, dvModuleInfo> dv::ModulesLoadLibrary(std::string_view moduleName) {
	// For each module, we search if a path exists to load it from.
	// If yes, we do so. The various OS's shared library load mechanisms
	// will keep track of reference count if same module is loaded
	// multiple times.
	boost::filesystem::path modulePath;

	{
		std::scoped_lock lock(glModuleData.modulePathsMutex);

		for (const auto &path : glModuleData.modulePaths) {
			if (moduleName == path.stem().string()) {
				// Found a module with same name!
				modulePath = path;
				break;
			}
		}
	}

	if (modulePath.empty()) {
		boost::format exMsg = boost::format("No module library for '%s' found.") % moduleName;
		throw std::runtime_error(exMsg.str());
	}

	return (InternalLoadLibrary(modulePath));
}

static std::pair<dv::ModuleLibrary, dvModuleInfo> InternalLoadLibrary(boost::filesystem::path modulePath) {
#if BOOST_HAS_DLL_LOAD
	dv::ModuleLibrary moduleLibrary;
	try {
		moduleLibrary.load(modulePath.c_str(), boost::dll::load_mode::rtld_now);
	}
	catch (const std::exception &ex) {
		// Failed to load shared library!
		boost::format exMsg
			= boost::format("Failed to load library '%s', error: '%s'.") % modulePath.string() % ex.what();
		throw std::system_error(std::error_code(), exMsg.str());
	}

	dvModuleInfo (*getInfo)(void);
	try {
		getInfo = moduleLibrary.get<dvModuleInfo(void)>("dvModuleGetInfo");
	}
	catch (const std::exception &ex) {
		// Failed to find symbol in shared library!
		dv::ModulesUnloadLibrary(moduleLibrary);
		boost::format exMsg
			= boost::format("Failed to find symbol in library '%s', error: '%s'.") % modulePath.string() % ex.what();
		throw std::range_error(exMsg.str());
	}
#else
	void *moduleLibrary = dlopen(modulePath.c_str(), RTLD_NOW);
	if (moduleLibrary == nullptr) {
		// Failed to load shared library!
		boost::format exMsg
			= boost::format("Failed to load library '%s', error: '%s'.") % modulePath.string() % dlerror();
		throw std::system_error(std::error_code(), exMsg.str());
	}

	dvModuleInfo (*getInfo)(void) = (dvModuleInfo(*)(void)) dlsym(moduleLibrary, "dvModuleGetInfo");
	if (getInfo == nullptr) {
		// Failed to find symbol in shared library!
		dv::ModulesUnloadLibrary(moduleLibrary);
		boost::format exMsg
			= boost::format("Failed to find symbol in library '%s', error: '%s'.") % modulePath.string() % dlerror();
		throw std::range_error(exMsg.str());
	}
#endif

	dvModuleInfo info = (*getInfo)();
	if (info == nullptr) {
		dv::ModulesUnloadLibrary(moduleLibrary);
		boost::format exMsg = boost::format("Failed to get info from library '%s'.") % modulePath.string();
		throw std::runtime_error(exMsg.str());
	}

	return (std::pair<dv::ModuleLibrary, dvModuleInfo>(moduleLibrary, info));
}

// Small helper to unload libraries on error.
void dv::ModulesUnloadLibrary(dv::ModuleLibrary &moduleLibrary) {
#if BOOST_HAS_DLL_LOAD
	moduleLibrary.unload();
#else
	dlclose(moduleLibrary);
#endif
}

void dv::ModulesUpdateInformationListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
	const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue) {
	UNUSED_ARGUMENT(userData);

	if (event == DVCFG_ATTRIBUTE_MODIFIED && changeType == DVCFG_TYPE_BOOL
		&& caerStrEquals(changeKey, "updateModulesInformation") && changeValue.boolean) {
		// Get information on available modules, put it into ConfigTree.
		ModulesUpdateInformation();

		dvConfigNodeAttributeBooleanReset(node, changeKey);
	}
}

static inline std::vector<boost::filesystem::path> findModuleCandidates(const boost::filesystem::path &searchPath) {
	std::vector<boost::filesystem::path> moduleCandidates;

	// Search is recursive for binary shared libraries.
#if defined(OS_MACOSX)
	const std::regex moduleRegex("\\w[\\w-]*\\.dylib");
#elif defined(OS_WINDOWS)
	const std::regex moduleRegex("\\w[\\w-]*\\.dll");
#else
	const std::regex moduleRegex("\\w[\\w-]*\\.so");
#endif

	if (boost::filesystem::exists(searchPath) && boost::filesystem::is_directory(searchPath)) {
		std::for_each(boost::filesystem::recursive_directory_iterator(searchPath),
			boost::filesystem::recursive_directory_iterator(),
			[&moduleRegex, &moduleCandidates](const boost::filesystem::directory_entry &e) {
				if (boost::filesystem::exists(e.path()) && boost::filesystem::is_regular_file(e.path())
					&& std::regex_match(e.path().filename().string(), moduleRegex)) {
					moduleCandidates.push_back(boost::filesystem::canonical(e.path()));
				}
			});
	}

	dv::vectorSortUnique(moduleCandidates);

	return (moduleCandidates);
}

static inline void cleanupModulesSearchPath(
	std::string searchCopy, const boost::filesystem::path &path, dv::Cfg::Node mNode) {
	// Ensure this is not part of the config setting.
	auto idx = searchCopy.find(path.string());

	// Not found.
	if (idx == std::string::npos) {
		return;
	}

	// Found, remove and update: +1 to also remove possible '|' char.
	searchCopy.erase(idx, path.string().length() + 1);

	mNode.put<dv::CfgType::STRING>("modulesSearchPath", searchCopy);
}

void dv::ModulesUpdateInformation() {
	std::scoped_lock lock(glModuleData.modulePathsMutex);

	auto modulesNode = dv::Cfg::GLOBAL.getNode("/system/modules/");

	// Clear out modules information.
	modulesNode.clearSubTree(false);
	modulesNode.removeSubTree();
	glModuleData.modulePaths.clear();

	// Search for available modules.
	std::vector<boost::filesystem::path> modules;

	// First all the additional paths in order.
	const std::string modulesSearchPath = modulesNode.get<dv::CfgType::STRING>("modulesSearchPath");

	// Split on '|'.
	boost::tokenizer<boost::char_separator<char>> additionalSearchPaths(
		modulesSearchPath, boost::char_separator<char>("|", nullptr));

	for (const auto &path : additionalSearchPaths) {
		const auto candidates = findModuleCandidates(path);

		modules.insert(modules.end(), candidates.begin(), candidates.end());
	}

	// Then the runtime location.
	char *execLocation = portable_get_executable_location();
	if (execLocation != nullptr) {
		boost::filesystem::path execPath{execLocation};
		free(execLocation);

		if (!boost::filesystem::is_directory(execPath)) {
			execPath.remove_filename();
		}

		execPath.append("dv_modules");

		const auto candidates = findModuleCandidates(execPath);

		modules.insert(modules.end(), candidates.begin(), candidates.end());

		cleanupModulesSearchPath(modulesSearchPath, execPath, modulesNode);
	}

	// And last the default system modules directory.
	{
		const boost::filesystem::path modulesDefaultDir{INTERNAL_XSTR(DV_MODULES_DIR)};

		const auto candidates = findModuleCandidates(modulesDefaultDir);

		modules.insert(modules.end(), candidates.begin(), candidates.end());

		cleanupModulesSearchPath(modulesSearchPath, modulesDefaultDir, modulesNode);
	}

	// Stable sort by file name, to support multiple modules with same name.
	std::stable_sort(
		modules.begin(), modules.end(), [](const boost::filesystem::path &a, const boost::filesystem::path &b) {
			return (a.filename() < b.filename());
		});

	// Remove any duplicates while preserving order.
	std::unordered_set<std::string> seen;

	for (const auto &path : modules) {
		if (!seen.count(path.string())) {
			seen.insert(path.string());

			glModuleData.modulePaths.push_back(path);
		}
	}

	glModuleData.modulePaths.shrink_to_fit();

	// Generate nodes for each module, with their in/out information as attributes.
	// This also checks basic validity of the module's information.
	auto iter = glModuleData.modulePaths.cbegin();

	while (iter != glModuleData.modulePaths.cend()) {
		const auto moduleName = iter->stem().string();

		// Disallow duplicates.
		if (modulesNode.existsRelativeNode(moduleName + "/")) {
			auto exMsg = boost::format("Module candidate '%s': removing duplicate '%s'.") % moduleName % iter->string();
			dv::Log(dv::logLevel::INFO, exMsg);

			iter = glModuleData.modulePaths.erase(iter);
			continue;
		}

		// Load library.
		std::pair<dv::ModuleLibrary, dvModuleInfo> mLoad;

		try {
			mLoad = InternalLoadLibrary(*iter);
		}
		catch (const std::range_error &ex) {
			auto exMsg = boost::format("Module candidate '%s': %s (probably not a DV module)") % moduleName % ex.what();
			dv::Log(dv::logLevel::DEBUG, exMsg);

			iter = glModuleData.modulePaths.erase(iter);
			continue;
		}
		catch (const std::exception &ex) {
			auto exMsg = boost::format("Module candidate '%s': %s") % moduleName % ex.what();
			dv::Log(dv::logLevel::ERROR, exMsg);

			iter = glModuleData.modulePaths.erase(iter);
			continue;
		}

		// Get node under /system/modules/.
		auto moduleNode = modulesNode.getRelativeNode(moduleName + "/");

		// Parse dvModuleInfo into ConfigTree.
		moduleNode.create<dv::CfgType::INT>("version", I32T(mLoad.second->version), {0, INT32_MAX},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Module version.");
		moduleNode.create<dv::CfgType::STRING>("description", mLoad.second->description, {1, 8192},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Module description.");
		moduleNode.create<dv::CfgType::STRING>("path", iter->string(), {1, PATH_MAX},
			dv::CfgFlags::READ_ONLY | dv::CfgFlags::NO_EXPORT, "Module file full path.");

		// Done, unload library.
		dv::ModulesUnloadLibrary(mLoad.first);

		iter++;
	}

	// No modules, cannot start!
	if (glModuleData.modulePaths.empty()) {
		dv::Log(dv::logLevel::ERROR, "Failed to find any modules (additional search paths = '%s').",
			modulesSearchPath.c_str());
	}
	else {
		dv::Log(dv::logLevel::DEBUG, "Found %d modules (additional search paths = '%s').",
			glModuleData.modulePaths.size(), modulesSearchPath.c_str());
	}
}
