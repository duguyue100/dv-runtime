#ifndef MODULE_HPP_
#define MODULE_HPP_

#include "dv-sdk/config.hpp"
#include "dv-sdk/module.h"

#include "log.hpp"
#include "modules_discovery.hpp"

#include <atomic>
#include <boost/lockfree/spsc_queue.hpp>
#include <condition_variable>
#include <mutex>
#include <string>
#include <string_view>
#include <thread>
#include <unordered_map>
#include <utility>

#define INTER_MODULE_TRANSFER_QUEUE_SIZE 256

namespace dv {

class Module;
class ModuleInput;
class ModuleOutput;

using TypedObjectSharedPtr = std::shared_ptr<dv::Types::TypedObject>;
using TypedObjectsSPSCQueue
	= boost::lockfree::spsc_queue<TypedObjectSharedPtr, boost::lockfree::capacity<INTER_MODULE_TRANSFER_QUEUE_SIZE>>;

class ModuleInput {
public:
	dv::Types::Type type;
	bool optional;
	ModuleOutput *linkedOutput;
	Module *parentModule;
	dv::Cfg::Node node;
	TypedObjectsSPSCQueue queue;
	std::vector<TypedObjectSharedPtr> inUsePackets;
	TypedObjectSharedPtr currentPublishedPacket;
	// Input data statistics.
	dv::_RateLimiter statRateLimiter;
	int64_t statPacketsNumber;
	int64_t statPacketsSize;

	ModuleInput(const dv::Types::Type &t, bool opt, Module *parentModule_, dv::Cfg::Node node_) :
		type(t),
		optional(opt),
		linkedOutput(nullptr),
		parentModule(parentModule_),
		node(node_),
		statRateLimiter(1, 200),
		statPacketsNumber(0),
		statPacketsSize(0) {
	}
};

struct InputDataAvailable {
public:
	// Input data availability.
	std::mutex lock;
	std::condition_variable cond;
	int32_t count;

	InputDataAvailable() : count(0) {
	}
};

class OutConnection {
public:
	TypedObjectsSPSCQueue *queue;
	InputDataAvailable *dataAvailable;
	ModuleInput *linkedInput;

	OutConnection(TypedObjectsSPSCQueue *queue_, InputDataAvailable *dataAvailable_, ModuleInput *linkedInput_) :
		queue(queue_),
		dataAvailable(dataAvailable_),
		linkedInput(linkedInput_) {
	}

	bool operator==(const OutConnection &rhs) const noexcept {
		// The linked input pointer is unique, so enough to establish equality.
		return (linkedInput == rhs.linkedInput);
	}

	bool operator!=(const OutConnection &rhs) const noexcept {
		return (!operator==(rhs));
	}
};

class ModuleOutput {
public:
	dv::Types::Type type;
	dv::Config::Node node;
	dv::Config::Node infoNode;
	Module *parentModule;
	std::mutex destinationsLock;
	std::vector<OutConnection> destinations;
	TypedObjectSharedPtr nextPacket;

	ModuleOutput(const dv::Types::Type &type_, dv::Config::Node node_, Module *parentModule_) :
		type(type_),
		node(node_),
		infoNode(node_.getRelativeNode("info/")),
		parentModule(parentModule_) {
	}
};

struct RunControl {
	// Run status.
	std::mutex lock;
	std::condition_variable cond;
	bool forcedShutdown;
	bool running;
	std::atomic_bool isRunning;
	std::atomic_bool configUpdate;
	bool runDelay;

	RunControl() : forcedShutdown(false), running(false), isRunning(false), configUpdate(false), runDelay(false) {
	}
};

class Module : public dvModuleDataS {
private:
	// Module info.
	std::string name;
	dvModuleInfo info;
	dv::ModuleLibrary library;
	dv::Config::Node moduleConfigNode;
	// Run status.
	struct RunControl run;
	// Logging.
	dv::LogBlock logger;
	// I/O connectivity.
	std::unordered_map<std::string, ModuleInput> inputs;
	std::unordered_map<std::string, ModuleOutput> outputs;
	// Input data availability.
	struct InputDataAvailable dataAvailable;
	size_t connectedInputs;
	// Module thread management.
	std::thread thread;
	std::atomic_bool threadAlive;

public:
	Module(std::string_view _name, std::string_view _library);
	~Module();

	void startThread();
	void stopThread();

	void registerType(const dv::Types::Type type);
	void registerOutput(std::string_view name, std::string_view typeName);
	void registerInput(std::string_view name, std::string_view typeName, bool optional = false);

	dv::Types::TypedObject *outputAllocate(std::string_view outputName);
	void outputCommit(std::string_view outputName);

	const dv::Types::TypedObject *inputGet(std::string_view inputName);
	void inputAdvance(std::string_view inputName);
	void inputDismiss(std::string_view inputName, const dv::Types::TypedObject *data);

	dv::Config::Node outputGetInfoNode(std::string_view outputName);
	const dv::Config::Node inputGetInfoNode(std::string_view inputName);
	bool inputIsConnected(std::string_view inputName);

private:
	void LoggingInit();
	void RunningInit();
	void StaticInit();

	Module *getModule(const std::string &moduleName);
	ModuleOutput *getModuleOutput(const std::string &outputName);
	ModuleInput *getModuleInput(const std::string &outputName);
	void _singleInputAdvance(std::string_view inputName);

	static void connectToModuleOutput(ModuleOutput *output, OutConnection connection);
	static void disconnectFromModuleOutput(ModuleOutput *output, OutConnection connection);

	void inputConnectivityInitialize();
	void inputConnectivityDisconnect();
	void inputConnectivityCleanup();

	void verifyOutputInfoNodes();
	void cleanupOutputInfoNodes();

	void runThread();
	void runStateMachine();
	void shutdownProcedure(bool doModuleExit, bool disableModule);
	void forcedShutdown(bool shutdown);

	static void moduleRunningListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue);
	static void moduleLogLevelListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue);
	static void moduleConfigUpdateListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue);
	static void moduleFromChangeListener(dvConfigNode node, void *userData, enum dvConfigAttributeEvents event,
		const char *changeKey, enum dvConfigAttributeType changeType, union dvConfigAttributeValue changeValue);
};

} // namespace dv

#endif /* MODULE_HPP_ */
