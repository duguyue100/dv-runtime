Summary: C++ event-based processing framework for neuromorphic cameras
Name:    dv-runtime
Version: VERSION_REPLACE
Release: 0%{?dist}
License: AGPLv3
URL:     https://gitlab.com/inivation/dv-runtime/
Vendor:  iniVation AG

Source0: https://gitlab.com/inivation/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: gcc >= 7.0, gcc-c++ >= 7.0, cmake >= 3.10, pkgconfig >= 0.29.0, libcaer-devel >= 3.2.2, boost-devel >= 1.62, openssl-devel, opencv-devel >= 3.2.0, gperftools-devel >= 2.4, SFML-devel >= 2.3.0, libX11-devel, lz4-devel, libzstd-devel
Requires: libcaer >= 3.2.2, boost >= 1.62, openssl, opencv >= 3.2.0, gperftools >= 2.4, SFML >= 2.3.0, libX11, lz4-libs, libzstd

%description
C++ event-based processing framework for neuromorphic cameras,
targeting embedded and desktop systems.

%package devel
Summary: C++ event-based processing framework for neuromorphic cameras (development files)
Requires: %{name}%{?_isa} = %{version}-%{release}, cmake >= 3.10, pkgconfig >= 0.29.0, libcaer-devel >= 3.2.2, boost-devel >= 1.62, openssl-devel, opencv-devel >= 3.2.0

%description devel
Development files for dv-runtime, such as headers, pkg-config files, etc.,
enabling users to write their own processing modules.

%prep
%autosetup

%build
%cmake -DENABLE_TCMALLOC=1 -DENABLE_VISUALIZER=1 .
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

%files
%{_bindir}/dv-runtime
%{_bindir}/dv-control
%{_bindir}/dv-filestat
%{_libdir}/libdvsdk.so.*
%{_datarootdir}/dv/modules/

%files devel
%{_includedir}/dv-sdk/
%{_datarootdir}/dv/dv-modules.cmake
%{_libdir}/libdvsdk.so
%{_libdir}/pkgconfig/
%{_libdir}/cmake/dv/

%changelog
* Fri Mar 23 2018 iniVation AG <support@inivation.com>
See ChangeLog file in source or GitLab.
