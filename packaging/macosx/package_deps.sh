#!/bin/bash

# Destination directory
mkdir -p dv-runtime-packaged-mac/dv_modules

# dv-runtime binary
cp src/dv-runtime dv-runtime-packaged-mac/
macpack -v -d dv_modules/libs dv-runtime-packaged-mac/dv-runtime

# dv-control binary
cp utils/dv-control/dv-control dv-runtime-packaged-mac/
macpack -v -d dv_modules/libs dv-runtime-packaged-mac/dv-control

# All available modules
for mod in $(find modules -type f -iname '*.dylib'); do
	echo "${mod}"
	cp "${mod}" dv-runtime-packaged-mac/dv_modules/
	macpack -v -d libs dv-runtime-packaged-mac/dv_modules/"$(basename ${mod})"
done
