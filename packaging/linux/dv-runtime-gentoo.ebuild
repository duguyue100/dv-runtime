# Copyright 2019 iniVation AG
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils cmake-utils

DESCRIPTION="C++ event-based processing framework for neuromorphic cameras, targeting embedded and desktop systems."
HOMEPAGE="https://gitlab.com/inivation/${PN}/"

SRC_URI="https://gitlab.com/inivation/${PN}/-/archive/${PV}/${P}.tar.gz"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="amd64 arm x86"
IUSE="debug old_visualizer"

RDEPEND=">=dev-libs/libcaer-3.2.2
	>=dev-libs/boost-1.62
	dev-libs/openssl
	>=media-libs/opencv-3.2.0
	>=app-arch/lz4-1.8.0
	>=app-arch/zstd-1.3.0
	>=dev-util/google-perftools-2.6
	old_visualizer? ( >=media-libs/libsfml-2.5.0 x11-libs/libX11 )"

DEPEND="${RDEPEND}
	virtual/pkgconfig
	>=dev-util/cmake-3.10.0"

src_configure() {
	local mycmakeargs=(
		-DENABLE_TCMALLOC=1
		-DENABLE_VISUALIZER="$(usex old_visualizer 1 0)"
	)

	cmake-utils_src_configure
}
