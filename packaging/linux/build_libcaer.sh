#!/bin/bash

mkdir libcaer_build
cd libcaer_build

git clone 'https://gitlab.com/inivation/libcaer.git' .

mkdir build
cd build

cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make -j2 -s
make install

cd ../..
rm -Rf libcaer_build
