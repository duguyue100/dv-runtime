ADD_EXECUTABLE(dv-tcpstat tcpstat.cpp)

TARGET_LINK_LIBRARIES(dv-tcpstat
	PRIVATE
		dvsdk
		Boost::boost
		Boost::system
		Boost::filesystem
		Boost::program_options
		${BOOST_ASIO_LIBRARIES})

INSTALL(TARGETS dv-tcpstat DESTINATION ${CMAKE_INSTALL_BINDIR})
