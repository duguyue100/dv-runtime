#include "dv-sdk/utils.h"

#include "../../modules/input/dv_input.hpp"

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>

namespace po = boost::program_options;

[[noreturn]] static inline void printHelpAndExit(const po::options_description &desc) {
	std::cout << std::endl << desc << std::endl;
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
	po::options_description cliDescription("Command-line options");
	cliDescription.add_options()("help,h", "print help text")("file,f", po::value<std::string>(), "File to examine")(
		"verbose,v", po::value<bool>(), "Print full packet table");

	po::positional_options_description p;
	p.add("file", -1);

	po::variables_map cliVarMap;
	try {
		po::store(po::command_line_parser(argc, argv).options(cliDescription).positional(p).run(), cliVarMap);
		po::notify(cliVarMap);
	}
	catch (...) {
		std::cout << "Failed to parse command-line options!" << std::endl;
		printHelpAndExit(cliDescription);
	}

	// Parse/check command-line options.
	if (cliVarMap.count("help")) {
		printHelpAndExit(cliDescription);
	}

	std::string file;
	if (cliVarMap.count("file")) {
		file = cliVarMap["file"].as<std::string>();
	}
	else {
		printHelpAndExit(cliDescription);
	}

	bool verbose = false;
	if (cliVarMap.count("verbose")) {
		verbose = cliVarMap["verbose"].as<bool>();
	}

	boost::filesystem::path filePath{file};

	std::ifstream fileStream;
	dv::InputInformation fileInfo;

	try {
		filePath = boost::filesystem::absolute(filePath);

		fileStream = dv::InputDecoder::openFile(filePath);

		fileInfo = dv::InputDecoder::parseHeader(fileStream, nullptr);
	}
	catch (const std::exception &ex) {
		auto exMsg = boost::format("File '%s': %s") % file % ex.what();
		std::cerr << exMsg.str() << std::endl;

		return (EXIT_FAILURE);
	}

	std::cout << "File path (absolute): " << filePath << std::endl;
	std::cout << "File path (canonical): " << boost::filesystem::canonical(filePath) << std::endl;
	std::cout << "File size (OS): " << boost::filesystem::file_size(filePath) << std::endl;
	std::cout << "File size (Parser): " << fileInfo.fileSize << std::endl;
	std::cout << "Compression: " << dv::EnumNameCompressionType(fileInfo.compression) << std::endl;
	std::cout << "Timestamp lowest: " << fileInfo.timeLowest << std::endl;
	std::cout << "Timestamp highest: " << fileInfo.timeHighest << std::endl;
	std::cout << "Timestamp difference: " << fileInfo.timeDifference << std::endl;
	std::cout << "Timestamp shift: " << fileInfo.timeShift << std::endl;

	if (fileInfo.streams.empty()) {
		std::cerr << "Streams: NONE FOUND" << std::endl;
	}
	else {
		for (const auto &st : fileInfo.streams) {
			auto msg = boost::format("Stream %d: %s - %s") % st.id % st.name % st.typeIdentifier;
			std::cout << msg.str() << std::endl;
		}
	}

	std::cout << "DataTable position: " << fileInfo.dataTablePosition << std::endl;
	std::cout << "DataTable size: " << fileInfo.dataTableSize << std::endl;

	if (fileInfo.dataTable->Table.empty()) {
		std::cerr << "DataTable: NONE FOUND" << std::endl;
	}
	else {
		if (verbose) {
			for (const auto &pkt : fileInfo.dataTable->Table) {
				auto msg
					= boost::format(
						  "Packet at %d: StreamID %d - Size %d - NumElements %d - TimestampStart %d - TimestampEnd %d")
					  % pkt.ByteOffset % pkt.PacketInfo.StreamID() % pkt.PacketInfo.Size() % pkt.NumElements
					  % pkt.TimestampStart % pkt.TimestampEnd;
				std::cout << msg.str() << std::endl;
			}
		}
	}

	return (EXIT_SUCCESS);
}
