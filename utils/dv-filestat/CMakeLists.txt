# Compression support
FIND_LIBRARY(LIBLZ4 lz4)
IF (${LIBLZ4-NOTFOUND})
	MESSAGE(FATAL_ERROR "liblz4 not found, required for I/O compression.")
ENDIF()

FIND_LIBRARY(LIBZSTD zstd)
IF (${LIBZSTD-NOTFOUND})
	MESSAGE(FATAL_ERROR "libzstd not found, required for I/O compression.")
ENDIF()

ADD_EXECUTABLE(dv-filestat filestat.cpp)

TARGET_LINK_LIBRARIES(dv-filestat
	PRIVATE
		dvsdk
		Boost::boost
		Boost::system
		Boost::filesystem
		Boost::program_options
		${LIBLZ4}
		${LIBZSTD})

INSTALL(TARGETS dv-filestat DESTINATION ${CMAKE_INSTALL_BINDIR})
